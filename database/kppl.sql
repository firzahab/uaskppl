-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 01, 2016 at 07:16 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kppl`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `aktivitas` varchar(100) NOT NULL,
  `waktu` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `username`, `aktivitas`, `waktu`) VALUES
(1, 'niniss', 'Log In', '2015-12-28 10:57:58'),
(2, 'niniss', 'Log out', '2015-12-28 11:07:38'),
(3, 'niniss', 'Log In', '2015-12-28 11:23:03'),
(4, 'niniss', 'Log out', '2015-12-28 11:23:08'),
(5, 'niniss', 'Log In', '2015-12-28 12:43:21'),
(6, 'niniss', 'Log out', '2015-12-31 18:25:26'),
(7, 'niniss', 'Log In', '2015-12-31 18:44:09'),
(8, 'niniss', 'Log out', '2015-12-31 18:45:01'),
(9, 'firzah', 'Log In', '2015-12-31 19:19:46'),
(10, 'firzah', 'Log out', '2015-12-31 19:19:54'),
(11, 'niswati', 'Log In', '2015-12-31 19:20:48'),
(12, 'niswati', 'Log out', '2015-12-31 19:20:57'),
(13, 'niswati', 'Log out', '2015-12-31 19:21:51'),
(14, 'niswati', 'Log In', '2015-12-31 19:22:12'),
(15, 'niswati', 'Log out', '2015-12-31 19:22:20'),
(16, 'sarah', 'Log In', '2015-12-31 19:33:24'),
(17, 'sarah', 'Log out', '2015-12-31 19:33:43'),
(18, 'oriehanna', 'Log In', '2015-12-31 19:51:40'),
(19, 'oriehanna', 'Log out', '2015-12-31 19:51:52'),
(20, 'niniss', 'Log In', '2015-12-31 19:59:57'),
(21, 'niniss', 'Log out', '2015-12-31 20:00:01'),
(22, 'oriehanna', 'Log In', '2015-12-31 20:05:26'),
(23, 'niniss', 'Log In', '2015-12-31 20:07:53'),
(24, 'niniss', 'Log out', '2015-12-31 20:07:56'),
(25, 'niniss', 'Log In', '2015-12-31 21:05:14'),
(26, 'null', 'Log out', '2015-12-31 21:08:08'),
(27, 'niniss', 'Log In', '2016-01-01 03:13:37'),
(28, 'niniss', 'Log out', '2016-01-01 03:13:45'),
(29, 'firzah', 'Log In', '2016-01-01 03:21:21'),
(30, 'firzah', 'Log out', '2016-01-01 03:21:25'),
(31, 'niniss', 'Log In', '2016-01-01 10:30:43'),
(32, 'niniss', 'Log In', '2016-01-01 10:34:30'),
(33, 'niniss', 'Log out', '2016-01-01 10:34:33'),
(34, 'niniss', 'Log In', '2016-01-01 10:55:42'),
(35, 'niniss', 'Log out', '2016-01-01 10:58:55'),
(36, 'firzah', 'Log In', '2016-01-01 11:39:29'),
(37, 'firzah', 'Log out', '2016-01-01 11:42:51'),
(38, 'niniss', 'Log In', '2016-01-01 11:48:35'),
(39, 'niniss', 'Log out', '2016-01-01 12:00:48'),
(40, 'firzah', 'Log In', '2016-01-01 12:01:07'),
(41, 'null', 'Submit Pelaporan', '2016-01-01 12:42:34'),
(42, 'firzah', 'Log In', '2016-01-01 12:44:06'),
(43, 'firzah', 'Log out', '2016-01-01 12:44:41'),
(44, 'niniss', 'Log In', '2016-01-01 12:46:31'),
(45, 'null', 'Submit Pelaporan', '2016-01-01 12:47:11'),
(46, 'ninis', 'Submit Pelaporan', '2016-01-01 12:53:43'),
(47, 'ghsa', 'Submit Pelaporan', '2016-01-01 13:08:08'),
(48, 'niniss', 'Log out', '2016-01-01 13:08:37');

-- --------------------------------------------------------

--
-- Table structure for table `formpelaporan`
--

CREATE TABLE `formpelaporan` (
  `id_pelaporan` int(100) NOT NULL,
  `nama_pelapor` varchar(100) NOT NULL,
  `nrp` varchar(10) NOT NULL,
  `nama_aset` varchar(100) NOT NULL,
  `kode_aset` varchar(20) NOT NULL,
  `eventdate` date NOT NULL,
  `ket_lapor` text NOT NULL,
  `statuslapor` varchar(30) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `formpelaporan`
--

INSERT INTO `formpelaporan` (`id_pelaporan`, `nama_pelapor`, `nrp`, `nama_aset`, `kode_aset`, `eventdate`, `ket_lapor`, `statuslapor`, `deleted`) VALUES
(8, 'Niswati Pusparinda    ', '5213100062', 'Monitor PC', 'M1001', '2015-12-26', 'rusak mbak', 'Sedang diproses', 0),
(9, 'Sarah Putri Ramadhani', '5213100185', 'Printer', 'P1000', '2015-12-26', 'Nyandet tintanya', 'menunggu', 0),
(10, 'Niswati', '5213100062', 'Monitor PC', 'M1003', '2016-01-01', 'Warnanya Biru', 'menunggu', 0),
(11, 'Niswati', '5213100062', 'Monitor PC', 'M1003', '2016-01-01', 'Warnanya Biru', 'menunggu', 0),
(12, 'Niswati', '5213100062', 'Monitor PC', 'M1004', '2016-01-01', 'Warnanya Biru', 'menunggu', 0),
(13, 'ninis', '5213100185', 'Monitor PC', 'M1001', '2016-01-01', 'warna kuning', 'menunggu', 0),
(14, 'ninis', '5213100062', 'Monitor PC', 'M1003', '2016-01-01', 'burem', 'menunggu', 0),
(15, 'ghsa', '5621', 'Monitor PC', '567', '2015-12-26', 'jkhgcbvnm,.', 'menunggu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id_barang` int(100) NOT NULL,
  `kodebarang` varchar(20) NOT NULL,
  `namabarang` varchar(100) NOT NULL,
  `jenisbarang` varchar(100) NOT NULL,
  `startdate` date NOT NULL,
  `expdate` date NOT NULL,
  `status` varchar(100) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id_barang`, `kodebarang`, `namabarang`, `jenisbarang`, `startdate`, `expdate`, `status`, `deleted`) VALUES
(1, 'M100', 'Monitor PC', 'Inventaris', '2015-12-20', '2025-12-25', 'Sangat Bagus', 1),
(2, 'M101', 'Monitor PC', 'Inventaris', '2015-12-20', '2025-12-25', 'Sangat Bagus', 1),
(3, 'M102 ', 'Monitor PC', 'Inventaris', '2015-12-20', '2016-12-20', 'Sangat Bagus', 0),
(4, 'P100', 'Printer Epson X100', 'Inventaris', '2015-12-20', '2025-12-20', 'Sangat Bagus', 0),
(5, 'P101', 'Printer Canon Z10', 'Inventaris', '2015-12-20', '2025-12-20', 'Sangat Bagus', 0),
(6, 'LC100', 'Layar LCD', 'Inventaris', '2015-12-20', '2025-12-20', 'Sangat Bagus', 0),
(7, 'LCP100', 'LCD Proyektor', 'Inventaris', '2015-12-20', '2025-12-20', 'Bagus', 1),
(8, 'FD100 ', 'FlashDisk', 'Inventaris', '2015-12-20', '2020-12-20', 'Sangat Bagus', 0),
(9, 'FD101 ', 'FlashDisk', 'Inventaris', '2011-12-20', '2021-12-20', 'Sangat Bagus', 0),
(10, 'HD100', 'HardDisk', 'Inventaris', '2015-12-20', '2025-12-20', 'Bagus', 0),
(11, 'HD101', 'HardDisk', 'Inventaris', '2014-12-20', '2019-12-20', 'Sangat Bagus', 0),
(12, 'SP100', 'Speaker', 'Inventaris', '2011-12-20', '2017-12-20', 'Bagus', 0),
(13, 'S100', 'Spidol', 'Barang Habis Pakai', '2015-06-20', '2015-12-20', 'Bagus', 0),
(14, 'TS100', 'Tinta Spidol', 'Barang Habis Pakai', '2015-08-25', '2016-01-20', 'Kurang Bagus', 0),
(15, '1001', 'Komputer', 'Inventaris', '2016-01-01', '2019-01-01', 'Sangat Bagus', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user` varchar(20) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nrp` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `notelp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user`, `Password`, `nama`, `nrp`, `email`, `notelp`) VALUES
('firzah', 'zaq', 'zq', '5213100069', 'firzahab@gmail.com', '0856454969967'),
('niniss', 'niniss', 'niswa', '5213100062', 'niswati13@mhs.is.its.ac.id', '085645496967'),
('niswati', 'niswati', 'niswatipuspa', '5213100070', 'niswati13@mhs.is.its.ac.id', '085645496967'),
('oriehanna', 'oriehanna', 'Oriehanna Esesiawati', '5213100029', 'orie@gmail.com', '08819730710'),
('sarah', 'sarah', 'Sarah Putri Ramadhani', '5213100185', 'sarahputrirmdhni@gmail.com', '08885155025');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `formpelaporan`
--
ALTER TABLE `formpelaporan`
  ADD PRIMARY KEY (`id_pelaporan`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id_barang`,`kodebarang`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user`,`nrp`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `formpelaporan`
--
ALTER TABLE `formpelaporan`
  MODIFY `id_pelaporan` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id_barang` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
