package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class about_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE HTML>\r\n");
      out.write("<!--\r\n");
      out.write("\tTwenty by HTML5 UP\r\n");
      out.write("\thtml5up.net | @n33co\r\n");
      out.write("\tFree for personal and commercial use under the CCA 3.0 license (html5up.net/license)\r\n");
      out.write("-->\r\n");
      out.write("<html>\r\n");
      out.write("\t<head>\r\n");
      out.write("\t\t<title>About Jurusan Sistem Informasi</title>\r\n");
      out.write("\t\t<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("\t\t<meta name=\"description\" content=\"\" />\r\n");
      out.write("\t\t<meta name=\"keywords\" content=\"\" />\r\n");
      out.write("\t\t<!--[if lte IE 8]><script src=\"css/ie/html5shiv.js\"></script><![endif]-->\r\n");
      out.write("\t\t<script src=\"js/jquery.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/jquery.dropotron.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/jquery.scrolly.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/jquery.scrollgress.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/skel.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/skel-layers.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/init.js\"></script>\r\n");
      out.write("\t\t<noscript>\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/skel.css\" />\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style.css\" />\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style-wide.css\" />\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style-noscript.css\" />\r\n");
      out.write("\t\t</noscript>\r\n");
      out.write("\t\t<!--[if lte IE 8]><link rel=\"stylesheet\" href=\"css/ie/v8.css\" /><![endif]-->\r\n");
      out.write("\t\t<!--[if lte IE 9]><link rel=\"stylesheet\" href=\"css/ie/v9.css\" /><![endif]-->\r\n");
      out.write("\t</head>\r\n");
      out.write("\t<body class=\"no-sidebar\">\r\n");
      out.write("\r\n");
      out.write("\t\t<!-- Header -->\r\n");
      out.write("\t\t\t<header id=\"header\" class=\"skel-layers-fixed\">\r\n");
      out.write("\t\t\t\t<h1 id=\"logo\"><a href=\"index.html\">Created by <span>Sarah Putri Ramadhani 5213100185</span></a></h1>\r\n");
      out.write("\t\t\t\t<nav id=\"nav\">\r\n");
      out.write("\t\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t\t<li class=\"current\"><a href=\"index.jsp\">Home</a></li>\r\n");
      out.write("\t\t\t\t\t\t<li class=\"submenu\">\r\n");
      out.write("\t\t\t\t\t\t\t<a href=\"\">Menu</a>\r\n");
      out.write("\t\t\t\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t\t\t\t<!--<li><a href=\"left-sidebar.html\">Left Sidebar</a></li>  Left Sidebar -->\r\n");
      out.write("\t\t\t\t\t\t\t\t<!--<li><a href=\"right-sidebar.html\">Right Sidebar</a></li> Right Sidebar -->\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"about.jsp\">Tentang JSI</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"cekinventory.jsp\">Daftar Asset</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"indexform2.jsp\">Formulir Pelaporan Asset</a></li> <!--No Sidebar -->\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"cekstatus.jsp\">Cek Status Pelaporan</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t\t\t<!--<li class=\"submenu\">\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"\">Submenu</a>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Dolore Sed</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Consequat</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Lorem Magna</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Sed Magna</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<li><a href=\"#\">Ipsum Nisl</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t\t\t\t\t</li> No Sidebar -->\r\n");
      out.write("\t\t\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t\t\t</li>\r\n");
      out.write("\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t</nav>\r\n");
      out.write("\t\t\t</header>\r\n");
      out.write("\r\n");
      out.write("\t\t<!-- Main -->\r\n");
      out.write("\t\t\t<article id=\"main\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<header class=\"special container\">\r\n");
      out.write("\t\t\t\t\t<span class=\"icon fa-mobile\"></span>\r\n");
      out.write("\t\t\t\t\t<h2>About<strong>Jurusan Sistem Informasi</strong></h2>\r\n");
      out.write("\t\t\t\t</header>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<!-- One -->\r\n");
      out.write("\t\t\t\t\t<section class=\"wrapper style4 container\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t\t<!-- Content -->\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"content\">\r\n");
      out.write("\t\t\t\t\t\t\t\t<section>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"image featured\"><img src=\"images/jsi.jpg\" alt=\"\" /></a>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<img src=\"images/logo.png\"  width=\"185\" height=\"90\" alt=\"\" />\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<img src=\"images/logo-its.png\" width=\"175\" height=\"95\" alt=\"\"/>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<header>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<h3>Sejarah</h3>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t</header>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<p>\r\n");
      out.write("\r\n");
      out.write("Jurusan Sistem Informasi, Fakultas Teknologi Informasi, Institut Teknologi Sepuluh Nopember ini berada di Sukolilo, Surabaya Utara. Dalam perjalanannya sampai saat ini, banyak yang telah dilalui dan bisa dituliskan sebagai penanda kemajuan SI | FTIF dengan tinta emas, seperti\r\n");
      out.write("\r\n");
      out.write("    <br/>Februari 2003, Mendapatkan hibah kompetisi SP4 dari DIKTI Kementerian Pendidikan Nasional RI\r\n");
      out.write("    <br/>September 2005, Pertama kali meluluskan sarjana sistem informasi.\r\n");
      out.write("    <br/>Agustus 2006, Mendapatkan akreditasi nasional dari BANPT dengan predikat A (sangat baik)\r\n");
      out.write("    <br/>Nopember 2006, Memperoleh peringkat pertama dalam ITS Award katagori C (jurusan baru sengan jumlah mahasiswa di bawah 300 orang)\r\n");
      out.write("    <br/>Maret 2007, Pertama kalinya mencetak majalah komunitas sistem informasi GengSi.\r\n");
      out.write("    <br/>Nopember 2007, Memperoleh penghargaan khusus jurusan produktifitas terbaik dalam ITS Award.\r\n");
      out.write("    <br/>Februari 2008, Peluncuran logo baru SI | FTIF\r\n");
      out.write("    <br/>Agustus 2008, Pertama kalinya mencetak Si Ways, sebuah rekaman jalan-jalan yang dipilih untuk mengembangkan jurusan.\r\n");
      out.write("    <br/>September 2008, Pertama kalinya joint research dengan perguruan tinggi luar negeri (Kumamoto University) yang diperoleh melalui program kerjasama ITS â JICA\r\n");
      out.write("    <br/>Nopember 2008, Menerbitkan pertama kalinya Jurnal Sisfo (sistem informasi)\r\n");
      out.write("    <br/>Desember 2008, Menyelenggarakan pertama kalinya seminar tingkat nasional dengan nama sesindo (seminar sistem informasi Indonesia)\r\n");
      out.write("    <br/>Januari 2009, Pertama kalinya membukukan laporan pencapaian kinerja jurusan sebagai pertanggung jawaban ketua dan seketaris jurusan kepada stakeholder.\r\n");
      out.write("    <br/>Maret 2009, Pertama kallinya menyelenggarakan recruitment (pegawai dosen dan karyawan) dengan menggunakan sistem on job training dan pembinaan.\r\n");
      out.write("    <br/>Mei 2009, Menyiarkan pertama kalinya SITV (sistem informasi televisi)\r\n");
      out.write("    <br/>Juni 2009, Mendirikan rintisan magister (S2) sistem informasi/teknologi informasi yang bekerjasama dengan program pasca sarjana teknik informatika FTIF ITS.\r\n");
      out.write("    <br/>Nopember 2009, Memperoleh penghargaan khusus jurusan dengan kerjasama terbanyak dalam ITS Award.\r\n");
      out.write("    <br/>Maret 2010, Terbentuknya forum orang tua mahasiswa sistem informasi (FORMASI)\r\n");
      out.write("    <br/>Maret 2010, Pertama kalinya diselenggarakan parent gathering. Ajang unjuk karya mahasiswa dihadapan para orang tua/walinya.\r\n");
      out.write("\r\n");
      out.write("</p>\r\n");
      out.write("\t\t\t\t\t\t\t\t</section>\r\n");
      out.write("\t\t\t\t\t\t\t</div>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t</section>\r\n");
      out.write("\r\n");
      out.write("\t\t<!-- Footer -->\r\n");
      out.write("\t\t\t<footer id=\"footer\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<ul class=\"icons\">\r\n");
      out.write("\t\t\t\t\t<li><a href=\"#\" class=\"icon circle fa-twitter\"><span class=\"label\">Twitter</span></a></li>\r\n");
      out.write("\t\t\t\t\t<li><a href=\"#\" class=\"icon circle fa-facebook\"><span class=\"label\">Facebook</span></a></li>\r\n");
      out.write("\t\t\t\t\t<li><a href=\"#\" class=\"icon circle fa-google-plus\"><span class=\"label\">Google+</span></a></li>\r\n");
      out.write("\t\t\t\t\t<li><a href=\"#\" class=\"icon circle fa-github\"><span class=\"label\">Github</span></a></li>\r\n");
      out.write("\t\t\t\t\t<li><a href=\"#\" class=\"icon circle fa-dribbble\"><span class=\"label\">Dribbble</span></a></li>\r\n");
      out.write("\t\t\t\t</ul>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<ul class=\"copyright\">\r\n");
      out.write("                                    <li><center>&copy; 2015 Institut Teknologi Sepuluh Nopember, Fakultas Teknologi Informasi, Jurusan Sistem Informasi</center></li>\r\n");
      out.write("\t\t\t\t</ul>\r\n");
      out.write("\r\n");
      out.write("\t\t\t</footer>\r\n");
      out.write("\r\n");
      out.write("\t</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
