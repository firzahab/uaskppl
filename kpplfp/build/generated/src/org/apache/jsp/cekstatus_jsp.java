package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class cekstatus_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE HTML>\r\n");
      out.write("\r\n");
      out.write("<!--\r\n");
      out.write("\tTwenty by HTML5 UP\r\n");
      out.write("\thtml5up.net | @n33co\r\n");
      out.write("\tFree for personal and commercial use under the CCA 3.0 license (html5up.net/license)\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("<?php\r\n");
      out.write("include \"connectcek.php\";\r\n");
      out.write("\r\n");
      out.write("?>\r\n");
      out.write("\r\n");
      out.write("\t<head>\r\n");
      out.write("\t\t<title>Cek Form Pelaporan</title>\r\n");
      out.write("\t\t<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("\t\t<meta name=\"description\" content=\"\" />\r\n");
      out.write("\t\t<meta name=\"keywords\" content=\"\" />\r\n");
      out.write("\t\t<!--[if lte IE 8]><script src=\"css/ie/html5shiv.js\"></script><![endif]-->\r\n");
      out.write("\t\t<script src=\"js/jquery.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/jquery.dropotron.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/jquery.scrolly.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/jquery.scrollgress.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/skel.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/skel-layers.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/init.js\"></script>\r\n");
      out.write("\t\t<noscript>\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/skel.css\" />\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style.css\" />\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style-wide.css\" />\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style-noscript.css\" />\r\n");
      out.write("\t\t</noscript>\r\n");
      out.write("\t\t<!--[if lte IE 8]><link rel=\"stylesheet\" href=\"css/ie/v8.css\" /><![endif]-->\r\n");
      out.write("\t\t<!--[if lte IE 9]><link rel=\"stylesheet\" href=\"css/ie/v9.css\" /><![endif]-->\r\n");
      out.write("\t\t\r\n");
      out.write("\t</head>\r\n");
      out.write("\t\r\n");
      out.write("\t<body class=\"no-sidebar\">\r\n");
      out.write("\t\r\n");
      out.write("\t\t<!-- Header -->\r\n");
      out.write("\t\t\t<header id=\"header\" class=\"skel-layers-fixed\">\r\n");
      out.write("\t\t\t\t<h1 id=\"logo\"><a href=\"index.html\">ASSET MANAGEMENT LABORATORIUM MANAJEMEN SISTEM INFORMASI</a></h1>\r\n");
      out.write("\t\t\t\t<nav id=\"nav\">\r\n");
      out.write("\t\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t\t<li class=\"current\"><a href=\"index.jsp\">Home</a></li>\r\n");
      out.write("\t\t\t\t\t\t<li class=\"submenu\">\r\n");
      out.write("\t\t\t\t\t\t\t<a href=\"\">Menu</a>\r\n");
      out.write("\t\t\t\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t\t\t\t<!--<li><a href=\"left-sidebar.html\">Left Sidebar</a></li>  Left Sidebar -->\r\n");
      out.write("\t\t\t\t\t\t\t\t<!--<li><a href=\"right-sidebar.html\">Right Sidebar</a></li> Right Sidebar -->\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"about.html\">About JSI</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"tempat.html\">Tempat JSI</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"jadwal.html\">Jadwal Pemakaian Tempat</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"indexform2.php\">Formulir Peminjaman Tempat</a></li> <!--No Sidebar -->\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"cekstatus.php\">Cek Status Peminjaman Tempat</a></li>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t\t\t</li>\r\n");
      out.write("\t\t\t\t\t\t<li><a href=\"login.jsp\" class=\"button special\">Login</a></li>\r\n");
      out.write("\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t</nav>\r\n");
      out.write("\t\t\t</header>\r\n");
      out.write("\r\n");
      out.write("\t\t<!-- Main -->\r\n");
      out.write("\r\n");
      out.write("\t\t\t<article id=\"main\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<header class=\"special container\">\r\n");
      out.write("\t\t\t\t\t<span class=\"icon fa-mobile\"></span>\r\n");
      out.write("\t\t\t\t\t<h2><strong>Cek Status Pelaporan</strong></h2>\r\n");
      out.write("\t\t\t\t</header>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<!-- One -->\r\n");
      out.write("\t\t\t\t\t<section class=\"wrapper style4 container2\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t\t<!-- Content -->\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"content\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<table id=\"tblData-table\" border=\"3%\" width=\"110%\" class=\"order-table\">\r\n");
      out.write("\t\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"10%\"><b><center>Nama Pelapor</center></b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"20%\"><b><center>NRP</center></b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"10%\"><b><center>Nama Asset</center></b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"10%\"><b><center>Kode Asset</center><b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"15%\"><b><center>Tanggal Pelaporan</center></b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"15%\"><b><center>Keterangan</center></b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"10%\"><b><center>Status</center></b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"30%\"><b><center>Opsi</center></b></td>\r\n");
      out.write("\t\t\t\t\t</tr>\r\n");
      out.write("\t\t\t\t\t<?php\r\n");
      out.write("\t\t\t\t\t\t$getrent=mysql_query(\"SELECT * FROM `formpelaporan`\");\r\n");
      out.write("\t\t\t\t\t\twhile($setstatusrent=mysql_fetch_array($getrent))\t\r\n");
      out.write("\t\t\t\t\t\t{\r\n");
      out.write("\t\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t?>\r\n");
      out.write("\t\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t\t<td><?php echo $setstatusrent['nama_pelapor'];?></td>\r\n");
      out.write("\t\t\t\t\t\t<td><?php echo $setstatusrent['nrp'];?></td>\r\n");
      out.write("\t\t\t\t\t\t<td><?php echo $setstatusrent['nama_aset'];?></td>\r\n");
      out.write("\t\t\t\t\t\t<td><?php echo $setstatusrent['kode_aset'];?></td>\r\n");
      out.write("\t\t\t\t\t\t<td><?php echo $setstatusrent['eventdate'];?></td>\r\n");
      out.write("\t\t\t\t\t\t<td><?php echo $setstatusrent['ket_lapor'];?></td>\r\n");
      out.write("\t\t\t\t\t\t<?php if ( isset( $_SESSION['is_admin'] ) && ! $setstatusrent['statuspinjam'] ) { ?>\r\n");
      out.write("\t\t\t\t\t\t<td>\r\n");
      out.write("\t\t\t\t\t\t\t<a class=\"buttonbro\" href=\"setujui.php?setuju=1&id=<?= $setstatusrent['id'] ?>\">Setuju</a>\r\n");
      out.write("\t\t\t\t\t\t\t<a class=\"buttonbro\" href=\"setujui.php?setuju=2&id=<?= $setstatusrent['id'] ?>\">Tolak</a>\r\n");
      out.write("\t\t\t\t\t\t</td>\r\n");
      out.write("\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t<?php } else { ?>\r\n");
      out.write("\t\t\t\t\t\t<td>\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"muncul-ini\">\r\n");
      out.write("\t\t\t\t\t\t<?php\r\n");
      out.write("\t\t\t\t\t\t\tif (  ! $setstatusrent['statuspinjam'] ) echo 'Masih diproses';\r\n");
      out.write("\t\t\t\t\t\t\telseif ( $setstatusrent['statuspinjam'] == 1 ) echo 'Disetujui';\r\n");
      out.write("\t\t\t\t\t\t\telse echo 'Ditolak'; ?>\r\n");
      out.write("\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t\t<?php } ?>\r\n");
      out.write("\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"muncul-ini\" style=\"display:none\">\r\n");
      out.write("\t\t\t\t\t\t\t\t<a class=\"buttonbro\" href=\"setujui.php?setuju=1&id=<?= $setstatusrent['id'] ?>\">Setuju</a>\r\n");
      out.write("\t\t\t\t\t\t\t\t<a class=\"buttonbro\" href=\"setujui.php?setuju=2&id=<?= $setstatusrent['id'] ?>\">Tolak</a>\r\n");
      out.write("\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t</td>\r\n");
      out.write("\t\t\t\t\t\t<td>\r\n");
      out.write("\t\t\t\t\t\t\t<a =class=\"buttonbro\" href=\"editcek.php?id=<?= $setstatusrent['id'] ?>\">Edit</a>\r\n");
      out.write("\t\t\t\t\t\t\t<a class=\"buttonbro\" href=\"del.php?id=<?= $setstatusrent['id'] ?>\">Delete</a>\r\n");
      out.write("\t\t\t\t\t\t</td>\r\n");
      out.write("\t\t\t\t\t</tr>\r\n");
      out.write("\t\t\t\t\t<?php\r\n");
      out.write("\t\t\t\t\t\t}\r\n");
      out.write("\t\t\t\t\t?>\r\n");
      out.write("\t\t\t\t</table>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t\t\r\n");
      out.write("\t\t\t<script>\r\n");
      out.write("\t\t\t\t$( '#klik-ini' ).click( function() {\r\n");
      out.write("\t\t\t\t\t$( this ).prev( '.muncul-ini' ).toggle();\r\n");
      out.write("\t\t\t\t} );\r\n");
      out.write("\t\t\t</script>\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
