package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import java.io.*;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;

public final class cekstatusadmin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE HTML>\r\n");
      out.write("\r\n");
      out.write("<!--\r\n");
      out.write("\tTwenty by HTML5 UP\r\n");
      out.write("\thtml5up.net | @n33co\r\n");
      out.write("\tFree for personal and commercial use under the CCA 3.0 license (html5up.net/license)\r\n");
      out.write("-->\r\n");
      out.write("\r\n");
      out.write("<html>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\t<head>\r\n");
      out.write("\t\t<title>Cek Form Pelaporan</title>\r\n");
      out.write("\t\t<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("\t\t<meta name=\"description\" content=\"\" />\r\n");
      out.write("\t\t<meta name=\"keywords\" content=\"\" />\r\n");
      out.write("\t\t<!--[if lte IE 8]><script src=\"css/ie/html5shiv.js\"></script><![endif]-->\r\n");
      out.write("\t\t<script src=\"js/jquery.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/jquery.dropotron.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/jquery.scrolly.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/jquery.scrollgress.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/skel.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/skel-layers.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/init.js\"></script>\r\n");
      out.write("\t\t<noscript>\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/skel.css\" />\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style.css\" />\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style-wide.css\" />\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style-noscript.css\" />\r\n");
      out.write("\t\t</noscript>\r\n");
      out.write("\t\t<!--[if lte IE 8]><link rel=\"stylesheet\" href=\"css/ie/v8.css\" /><![endif]-->\r\n");
      out.write("\t\t<!--[if lte IE 9]><link rel=\"stylesheet\" href=\"css/ie/v9.css\" /><![endif]-->\r\n");
      out.write("\t\t\r\n");
      out.write("\t</head>\r\n");
      out.write("\t\r\n");
      out.write("\t<body class=\"no-sidebar\">\r\n");
      out.write("\t\r\n");
      out.write("\t\t<!-- Header -->\r\n");
      out.write("\t\t\t<header id=\"header\" class=\"skel-layers-fixed\">\r\n");
      out.write("\t\t\t\t<h1 id=\"logo\"><a href=\"index.html\">ASSET MANAGEMENT LABORATORIUM MANAJEMEN SISTEM INFORMASI</a></h1>\r\n");
      out.write("\t\t\t\t<nav id=\"nav\">\r\n");
      out.write("\t\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t\t<li class=\"current\"><a href=\"index.jsp\">Home</a></li>\r\n");
      out.write("\t\t\t\t\t\t<li class=\"submenu\">\r\n");
      out.write("\t\t\t\t\t\t\t<a href=\"\">Menu</a>\r\n");
      out.write("\t\t\t\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t\t\t\t<!--<li><a href=\"left-sidebar.html\">Left Sidebar</a></li>  Left Sidebar -->\r\n");
      out.write("\t\t\t\t\t\t\t\t<!--<li><a href=\"right-sidebar.html\">Right Sidebar</a></li> Right Sidebar -->\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"cekinventoryadmin.jsp\">Daftar Aset</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"cekstatusadmin.jsp\">Cek Status Pelaporan</a></li>\r\n");
      out.write("                                                                <li><a href=\"record.jsp\">Record Activity</a></li>\r\n");
      out.write("                                                                \r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t\t\t</li>\r\n");

if ((session.getAttribute("username") == null)) {

      out.write("\r\n");
      out.write("<li><a href=\"loginadmin.jsp\" class=\"button special\">Login</a></li>\r\n");
} else {

      out.write("\r\n");
      out.write("<li><a href=\"./loginadmin/logoutadmin.jsp\" class=\"button special\">Logout</a></li>\r\n");

    }

      out.write("\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t</nav>\r\n");
      out.write("\t\t\t</header>\r\n");
      out.write("\r\n");
      out.write("\t\t<!-- Main -->\r\n");
      out.write("\r\n");
      out.write("\t\t\t<article id=\"main\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<header class=\"special container\">\r\n");
      out.write("\t\t\t\t\t<span class=\"icon fa-mobile\"></span>\r\n");
      out.write("\t\t\t\t\t<h2><strong>Cek Status Pelaporan</strong></h2>\r\n");
      out.write("\t\t\t\t</header>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<!-- One -->\r\n");
      out.write("\t\t\t\t\t<section class=\"wrapper style4 container2\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t\t<!-- Content -->\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"content\">\r\n");

try {
 
//deklarasi url database
 String url = "jdbc:mysql://localhost:3306/kppl";
 Connection con = null;
 Statement stat = null;
 ResultSet rs = null;
 
//load jdbc driver
 Class.forName("com.mysql.jdbc.Driver");
 
con = DriverManager.getConnection(url, "root", "");
 
stat = con.createStatement();
 
//membuat query
 String query = "select * from formpelaporan where deleted=0";
 
rs = stat.executeQuery(query);
 

      out.write("\r\n");
      out.write("                                                            \r\n");
      out.write("\t\t\t\t<table id=\"tblData-table\" border=\"3%\" width=\"110%\" class=\"order-table\">\r\n");
      out.write("\t\t\t\t\t<tr>\r\n");
      out.write("                                                <td width=\"5%\"><b><center>No.</center></b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"15%\"><b><center>Nama Pelapor</center></b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"15%\"><b><center>NRP</center></b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"10%\"><b><center>Nama Asset</center></b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"7%\"><b><center>Kode Asset</center><b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"10%\"><b><center>Tanggal Pelaporan</center></b></td>\r\n");
      out.write("\t\t\t\t\t\t<td width=\"15%\"><b><center>Keterangan</center></b></td>\r\n");
      out.write("                                                <td width=\"10%\"><b><center>Status Pelaporan</center></b></td>\r\n");
      out.write("                                                <td width=\"15%\"><b><center>Opsi</center></b></td>\r\n");
      out.write("\t\t\t\t\t</tr>\r\n");
      out.write(" ");
 while (rs.next())
 {
 
      out.write("\r\n");
      out.write("\t\t\t\t\t<tr>\r\n");
      out.write("\t\t\t\t\t\t<td>");
      out.print(rs.getString(1));
      out.write("</td>\r\n");
      out.write("\t\t\t\t\t\t<td>");
      out.print(rs.getString(2));
      out.write("</td>\r\n");
      out.write("\t\t\t\t\t\t<td>");
      out.print(rs.getString(3));
      out.write("</td>\r\n");
      out.write("\t\t\t\t\t\t<td>");
      out.print(rs.getString(4));
      out.write("</td>\r\n");
      out.write("\t\t\t\t\t\t<td>");
      out.print(rs.getString(5));
      out.write("</td>\r\n");
      out.write("\t\t\t\t\t\t<td>");
      out.print(rs.getString(6));
      out.write("</td>\r\n");
      out.write("                                                <td>");
      out.print(rs.getString(7));
      out.write("</td>\r\n");
      out.write("                                                <td>");
      out.print(rs.getString(8));
      out.write("</td>\r\n");
      out.write("                                                <td>\r\n");
      out.write("                                                    <a class=\"buttonbro\" href=\"editform.jsp?u=");
      out.print(rs.getString(1));
      out.write("\"> Edit</a>\r\n");
      out.write("                                                    <a class=\"buttonbro\" href=\"./editdelete/deleteform.jsp?u=");
      out.print(rs.getString(1));
      out.write("\"> Delete</a>\r\n");
      out.write("                                                </td>\r\n");
      out.write("\t\t\t\t\t</tr>\r\n");

 }
 
      out.write("\r\n");
      out.write(" ");

     
  //menutup koneksi
 rs.close();
 stat.close();
 con.close();
 }
 catch (Exception ex)
 {
 out.println ("Unable to connect to database");
 }
 
      out.write("\r\n");
      out.write("\t\t\t</table>\r\n");
      out.write("\t\t\t</div>\r\n");
      out.write("\t\t\t\t\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
