package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class indexadmin_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html>\r\n");
      out.write("\t<head>\r\n");
      out.write("\t\t<title>Sistem Informasi</title>\r\n");
      out.write("\t\t<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("\t\t<meta name=\"description\" content=\"\" />\r\n");
      out.write("\t\t<meta name=\"keywords\" content=\"\" />\r\n");
      out.write("\t\t<!--[if lte IE 8]><script src=\"css/ie/html5shiv.js\"></script><![endif]-->\r\n");
      out.write("\t\t<script src=\"js/jquery.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/jquery.dropotron.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/jquery.scrolly.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/jquery.scrollgress.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/skel.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/skel-layers.min.js\"></script>\r\n");
      out.write("\t\t<script src=\"js/init.js\"></script>\r\n");
      out.write("\t\t<noscript>\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/skel.css\" />\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style.css\" />\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style-wide.css\" />\r\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style-noscript.css\" />\r\n");
      out.write("\t\t</noscript>\r\n");
      out.write("\t\t<!--[if lte IE 8]><link rel=\"stylesheet\" href=\"css/ie/v8.css\" /><![endif]-->\r\n");
      out.write("\t\t<!--[if lte IE 9]><link rel=\"stylesheet\" href=\"css/ie/v9.css\" /><![endif]-->\r\n");
      out.write("\t</head>\r\n");
      out.write("\t<body class=\"index\">\r\n");
      out.write("\r\n");
      out.write("\t\t<!-- Header -->\r\n");
      out.write("\t\t\t<header id=\"header\" class=\"alt\">\r\n");
      out.write("\t\t\t\t<h1 id=\"logo\"><a href=\"index.html\">Asset Management Laboratorium Manajemen Sistem Informasi</a></h1>\r\n");
      out.write("\t\t\t\t<nav id=\"nav\">\r\n");
      out.write("\t\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t\t<li class=\"current\"><a href=\"index.html\">Home</a></li>\r\n");
      out.write("\t\t\t\t\t\t<li class=\"submenu\">\r\n");
      out.write("\t\t\t\t\t\t\t<a href=\"\">Menu</a>\r\n");
      out.write("\t\t\t\t\t\t\t<ul>\r\n");
      out.write("\t\t\t\t\t\t\t\t<!--<li><a href=\"left-sidebar.html\">Left Sidebar</a></li>  Left Sidebar -->\r\n");
      out.write("\t\t\t\t\t\t\t\t<!--<li><a href=\"right-sidebar.html\">Right Sidebar</a></li> Right Sidebar -->\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"about.jsp\">Tentang JSI</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"tempat.jsp\">Daftar Asset</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"indexform2.jsp\">Formulir Pelaporan Asset</a></li> <!--No Sidebar -->\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"cekstatus.jsp\">Cek Status Pelaporan</a></li>\r\n");
      out.write("                                                                <li><a href=\"record.jsp\">Record Activity</a></li>\r\n");
      out.write("                                                                <li><a href=\"report.jsp\">Report</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n");
      out.write("\t\r\n");
      out.write("\t\t\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t\t\t</li> \r\n");
      out.write("                                                \r\n");

if ((session.getAttribute("username") == null)) {

      out.write("\r\n");
      out.write("<li><a href=\"loginadmin.jsp\" class=\"button special\">Login</a></li>\r\n");
} else {

      out.write("\r\n");
      out.write("<li><a href=\"./loginadmin/logoutadmin.jsp\" class=\"button special\">Logout</a></li>\r\n");

    }

      out.write("\r\n");
      out.write("                                                \r\n");
      out.write("\t\t\t\t\t\t\r\n");
      out.write("\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t</nav>\r\n");
      out.write("\t\t\t</header>\r\n");
      out.write("\r\n");
      out.write("\t\t<!-- Banner -->\r\n");
      out.write("\t\t\t<section id=\"banner\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<div class=\"inner\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t<header>\r\n");
      out.write("\t\t\t\t\t\t<h2>JURUSAN SISTEM INFORMASI</h2>\r\n");
      out.write("\t\t\t\t\t</header>\r\n");
      out.write("\t\t\t\t\t<p>Asset Management <strong> Laboratorium Manajemen Sistem Informasi</strong> \r\n");
      out.write("\t\t\t\t\t<br/>\r\n");
      out.write("\t\t\t\t\tJurusan Sistem Informasi Institut Teknologi Sepuluh Nopember\r\n");
      out.write("\t\t\t\t\t<br />\r\n");
      out.write("\t\t\t\t\tNeed More Info?\r\n");
      out.write("\t\t\t\t\t<br />\r\n");
      out.write("\t\r\n");
      out.write("\t\t\t\t\t<footer>\r\n");
      out.write("\t\t\t\t\t\t<ul class=\"buttons vertical\">\r\n");
      out.write("\t\t\t\t\t\t\t<li><a href=\"#main\" class=\"button fit scrolly\">Tell Me More ! :)</a></li>\r\n");
      out.write("\t\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t\t</footer>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t</div>\r\n");
      out.write("\r\n");
      out.write("\t\t\t</section>\r\n");
      out.write("\r\n");
      out.write("\t\t<!-- Main -->\r\n");
      out.write("\t\t\t<article id=\"main\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<header class=\"special container\">\r\n");
      out.write("\t\t\t\t\t<span class=\"icon fa-bar-chart-o\"></span>\r\n");
      out.write("\t\t\t\t\t<h2>Apa itu <strong>Sistem Informasi Asset Management?</strong\r\n");
      out.write("\t\t\t\t\t<br />\r\n");
      out.write("\t\t\t\t\t</h2>\r\n");
      out.write("\t\t\t\t\t<p>Sistem Informasi ini akan memudahkan Mahasiswa Lab MSI Jurusan Sistem Informasi Institut Teknologi Sepuluh Nopember\r\n");
      out.write("\t\t\t\t\t<br />\r\n");
      out.write("\t\t\t\t\tuntuk mengetahui daftar aset yang dimiliki Lab MSI \r\n");
      out.write("\t\t\t\t\t<br />\r\n");
      out.write("\t\t\t\t\tdan mahasiswa dapat melaporkan kondisi aset secara online.\r\n");
      out.write("\t\t\t\t\t<br/>\r\n");
      out.write("\t\t\t\t\tUntuk pelaporan, mahasiswa harus\r\n");
      out.write("\t\t\t\t\t<a href=\"login.html\">Login</a> terlebih dahulu. Selamat mencoba!</p>\r\n");
      out.write("\t\t\t\t</header>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<!-- Three -->\r\n");
      out.write("\t\t\t\t\t<section class=\"wrapper style3 container special\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t\t<header class=\"major\">\r\n");
      out.write("\t\t\t\t\t\t\t<h2>Mari intip <strong>Dua Jenis Aset yang ada di Lab Manajemen Sistem Informasi di JSI</strong></h2>\r\n");
      out.write("\t\t\t\t\t\t</header>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t\t<div class=\"row\">\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"6u 12u(narrower)\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t\t\t\t<section>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"image featured\"><img src=\"images/kelas.jpg\" alt=\"\" /></a>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<header>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<h3>Inventaris</h3>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t</header>\r\n");
      out.write("                                                                        <p>Jenis aset <strong>inventaris </strong> merupakan aset yang memiliki jangka waktu penggunaan yang lama. Contoh jenis aset inventoris antara lain Komputer, Meja, Kursi, LCD, Layar Proyektor, Microphone, Speaker, dll.</p>\r\n");
      out.write("\t\t\t\t\t\t\t\t</section>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\t<div class=\"6u 12u(narrower)\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t\t\t\t<section>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<a href=\"#\" class=\"image featured\"><img src=\"images/aula.jpg\" alt=\"\" /></a>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t<header>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<h3>Barang Habis Pakai</h3>\r\n");
      out.write("\t\t\t\t\t\t\t\t\t</header>\r\n");
      out.write("                                                                        <p>Jenis aset <strong>Barang habis pakai </strong>merupakan aset yang memiliki jangkau waktu penggunaan yang pendek. Contoh jenis aset habis pakai antara lain Kertas, Alat Tulis Kantor (ATK), dll.</p>\r\n");
      out.write("\t\t\t\t\t\t\t\t</section>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t</div>\r\n");
      out.write("\t\t\t\t\t\t\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t\t<footer class=\"major\">\r\n");
      out.write("\t\t\t\t\t\t\t<ul class=\"buttons\">\r\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"tempat.html\" class=\"button\">See More</a></li>\r\n");
      out.write("\t\t\t\t\t\t\t</ul>\r\n");
      out.write("\t\t\t\t\t\t</footer>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t\t</section>\r\n");
      out.write("\r\n");
      out.write("\t\t\t</article>\r\n");
      out.write("\r\n");
      out.write("\t\t<!-- Footer -->\r\n");
      out.write("\t\t\t<footer id=\"footer\">\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<ul class=\"icons\">\r\n");
      out.write("\t\t\t\t\t<li><a href=\"#\" class=\"icon circle fa-twitter\"><span class=\"label\">Twitter</span></a></li>\r\n");
      out.write("\t\t\t\t\t<li><a href=\"#\" class=\"icon circle fa-facebook\"><span class=\"label\">Facebook</span></a></li>\r\n");
      out.write("\t\t\t\t\t<li><a href=\"#\" class=\"icon circle fa-google-plus\"><span class=\"label\">Google+</span></a></li>\r\n");
      out.write("\t\t\t\t\t<li><a href=\"#\" class=\"icon circle fa-github\"><span class=\"label\">Github</span></a></li>\r\n");
      out.write("\t\t\t\t\t<li><a href=\"#\" class=\"icon circle fa-dribbble\"><span class=\"label\">Dribbble</span></a></li>\r\n");
      out.write("\t\t\t\t</ul>\r\n");
      out.write("\r\n");
      out.write("\t\t\t\t<ul class=\"copyright\">\r\n");
      out.write("\t\t\t\t\t<li>&copy; 2015 Institut Teknologi Sepuluh Nopember, Fakultas Teknologi Informasi, Jurusan Sistem Informasi<br/>Created by<br/><br/>Sarah Putri Ramadhani 5213100185</center></li>\r\n");
      out.write("\t\t\t\t</ul>\r\n");
      out.write("\r\n");
      out.write("\t\t\t</footer>\r\n");
      out.write("\r\n");
      out.write("\t</body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
