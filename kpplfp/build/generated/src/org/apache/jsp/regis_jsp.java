package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class regis_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE HTML>\n");
      out.write("\n");
      out.write("<!--\n");
      out.write("\tTwenty by HTML5 UP\n");
      out.write("\thtml5up.net | @n33co\n");
      out.write("\tFree for personal and commercial use under the CCA 3.0 license (html5up.net/license)\n");
      out.write("-->\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\t<head>\n");
      out.write("\t\t<title>Login dan Registrasi</title>\n");
      out.write("\t\t<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />\n");
      out.write("\t\t<meta name=\"description\" content=\"\" />\n");
      out.write("\t\t<meta name=\"keywords\" content=\"\" />\n");
      out.write("\t\t<!--[if lte IE 8]><script src=\"css/ie/html5shiv.js\"></script><![endif]-->\n");
      out.write("\t\t<script src=\"js/jquery.min.js\"></script>\n");
      out.write("\t\t<script src=\"js/jquery.dropotron.min.js\"></script>\n");
      out.write("\t\t<script src=\"js/jquery.scrolly.min.js\"></script>\n");
      out.write("\t\t<script src=\"js/jquery.scrollgress.min.js\"></script>\n");
      out.write("\t\t<script src=\"js/skel.min.js\"></script>\n");
      out.write("\t\t<script src=\"js/skel-layers.min.js\"></script>\n");
      out.write("\t\t<script src=\"js/init.js\"></script>\n");
      out.write("\t\t<noscript>\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/skel.css\" />\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style.css\" />\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style-wide.css\" />\n");
      out.write("\t\t\t<link rel=\"stylesheet\" href=\"css/style-noscript.css\" />\n");
      out.write("\t\t</noscript>\n");
      out.write("\t\t<!--[if lte IE 8]><link rel=\"stylesheet\" href=\"css/ie/v8.css\" /><![endif]-->\n");
      out.write("\t\t<!--[if lte IE 9]><link rel=\"stylesheet\" href=\"css/ie/v9.css\" /><![endif]-->\n");
      out.write("\t\t\n");
      out.write("\t</head>\n");
      out.write("\t\n");
      out.write("\t<body class=\"no-sidebar\">\n");
      out.write("\t\n");
      out.write("\t\t<!-- Header -->\n");
      out.write("\t\t\t<header id=\"header\" class=\"skel-layers-fixed\">\n");
      out.write("\t\t\t\t<h1 id=\"logo\"><a href=\"index.html\">Created by <span>Sarah Putri Ramadhani 5213100185</span></a></h1>\n");
      out.write("\t\t\t\t<nav id=\"nav\">\n");
      out.write("\t\t\t\t\t<ul>\n");
      out.write("\t\t\t\t\t\t<li class=\"current\"><a href=\"index.jsp\">Home</a></li>\n");
      out.write("\t\t\t\t\t\t<li class=\"submenu\">\n");
      out.write("\t\t\t\t\t\t\t<a href=\"\">Menu</a>\n");
      out.write("\t\t\t\t\t\t\t<ul>\n");
      out.write("\t\t\t\t\t\t\t\t<!--<li><a href=\"left-sidebar.html\">Left Sidebar</a></li>  Left Sidebar -->\n");
      out.write("\t\t\t\t\t\t\t\t<!--<li><a href=\"right-sidebar.html\">Right Sidebar</a></li> Right Sidebar -->\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"about.jsp\">Tentang JSI</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"tempat.jsp\">Daftar Asset</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"indexform2.jsp\">Formulir Pelaporan Asset</a></li> <!--No Sidebar -->\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"cekstatus.jsp\">Cek Status Pelaporan</a></li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"login.jsp\" class=\"button special\">Login</a></li>\n");
      out.write("                                               \n");
      out.write("\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t</nav>\n");
      out.write("\t\t\t</header>\n");
      out.write("\n");
      out.write("\t\t<!-- Main -->\n");
      out.write("\n");
      out.write("\t\t<article id=\"main\">\n");
      out.write("\t\t\t\t<header class=\"special container\">\n");
      out.write("\t\t\t\t\t<span class=\"icon fa-mobile\"></span>\n");
      out.write("\t\t\t\t\t<h2><strong>Registrasikan diri anda sekarang!</strong></h2>\n");
      out.write("\t\t\t\t</header>\n");
      out.write("\n");
      out.write("\t\t\t\t<!-- One -->\n");
      out.write("\t\t\t\t\t<section class=\"wrapper style4 container2\">\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t<!-- Content -->\n");
      out.write("\t\t\t\t\t\t\t        <div class=\"container\">\n");
      out.write("\n");
      out.write("\n");
      out.write("        <div class=\"row\">\n");
      out.write("        <div class=\"span6\">\n");
      out.write("            \n");
      out.write("            <form class=\"form-horizontal well\" action=\"login/registration.jsp\" method=\"post\">\n");
      out.write("            <fieldset>\n");
      out.write("                <H2>Registrasi</H2>\n");
      out.write("                <div class=\"control-group\">\n");
      out.write("                    <label class=\"control-label\" for=\"uname\">Nama lengkap</label>\n");
      out.write("\n");
      out.write("                    <div class=\"controls\">\n");
      out.write("                        <input type=\"text\" id=\"inputemail\" placeholder=\"nama lengkap\">\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"control-group\">\n");
      out.write("                    <label class=\"control-label\" for=\"pass\">NRP</label>\n");
      out.write("\n");
      out.write("                    <div class=\"controls\">\n");
      out.write("                        <input type=\"password\" id=\"pass\" placeholder=\"NRP\">\n");
      out.write("                    </div>\n");
      out.write("                    </div>\n");
      out.write("                <div class=\"control-group\">\n");
      out.write("                    <label class=\"control-label\" for=\"pass\">Nomor Telepon</label>\n");
      out.write("\n");
      out.write("                    <div class=\"controls\">\n");
      out.write("                        <input type=\"password\" id=\"pass\" placeholder=\"nomor telepon\">\n");
      out.write("                    </div>\n");
      out.write("                    </div>\n");
      out.write("                <div class=\"control-group\">\n");
      out.write("                    <label class=\"control-label\" for=\"pass\">Email</label>\n");
      out.write("\n");
      out.write("                    <div class=\"controls\">\n");
      out.write("                        <input type=\"password\" id=\"pass\" placeholder=\"email\">\n");
      out.write("                    </div>\n");
      out.write("                    </div>\n");
      out.write("                <div class=\"control-group\">\n");
      out.write("                    <label class=\"control-label\" for=\"pass\">Password</label>\n");
      out.write("\n");
      out.write("                    <div class=\"controls\">\n");
      out.write("                        <input type=\"password\" id=\"pass\" placeholder=\"Password\">\n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"control-group\">\n");
      out.write("                    <div class=\"controls\">\n");
      out.write("                        \n");
      out.write("                    </div>\n");
      out.write("                </div>\n");
      out.write("                <div class=\"form-actions\">\n");
      out.write("                    <input type=\"submit\" class=\"button special\" value=\"Register\" /> \n");
      out.write("                    <!--<button type=\"submit\" class=\"btn btn-primary\">Login</button>-->\n");
      out.write("                   <!-- <button type=\"button\" class=\"btn\">Cancel</button>-->\n");
      out.write("                </div>\n");
      out.write("                \n");
      out.write("            </fieldset>\n");
      out.write("        </form>\n");
      out.write("    </div>\n");
      out.write("        </div>\n");
      out.write("\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t\t\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
