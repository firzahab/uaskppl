<!DOCTYPE HTML>
<!--
        Twenty by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>About Jurusan Sistem Informasi</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.dropotron.min.js"></script>
        <script src="js/jquery.scrolly.min.js"></script>
        <script src="js/jquery.scrollgress.min.js"></script>
        <script src="js/skel.min.js"></script>
        <script src="js/skel-layers.min.js"></script>
        <script src="js/init.js"></script>
        <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
        <link rel="stylesheet" href="css/style-noscript.css" />
        </noscript>

    </head>
    <body class="no-sidebar">

        <!-- Header -->
        <header id="header" class="skel-layers-fixed">
            <h1 id="logo"><a href="index.jsp">ASSET MANAGEMENT LABORATORIUM MANAJEMEN SISTEM INFORMASI</a></h1>
            <nav id="nav">
                <ul>
                    <li class="current"><a href="index.jsp">Home</a></li>
                    <li class="submenu">
                        <a href="">Menu</a>
                        <ul>

                            <li><a href="about.jsp">Tentang JSI</a></li>
                            <li><a href="cekinventory.jsp">Daftar Aset</a></li>
                            <li><a href="indexform2.jsp">Formulir Pelaporan Aset</a></li> <!--No Sidebar -->
                            <li><a href="cekstatus.jsp">Cek Status Pelaporan</a></li>


                        </ul>
                    </li>
                    <%
                        if ((session.getAttribute("user") == null)) {
                    %>
                    <li><a href="login.jsp" class="button special">Login</a></li>
                        <%} else {
                        %>
                    <li><a href="./login/logout.jsp" class="button special">Logout</a></li>
                        <%
                            }
                        %>						
                </ul>
            </nav>
        </header>

        <!-- Main -->
        <article id="main">

            <header class="special container">
                <span class="icon fa-mobile"></span>
                <h2>About<strong>Jurusan Sistem Informasi</strong></h2>
            </header>

            <!-- One -->
            <section class="wrapper style4 container">

                <!-- Content -->
                <div class="content">
                    <section>
                        <a href="#" class="image featured"><img src="images/jsi.jpg" alt="" /></a>
                        <img src="images/logo.png"  width="185" height="90" alt="" />
                        <img src="images/logo-its.png" width="175" height="95" alt=""/>
                        <header>
                            <h3>Sejarah</h3>
                        </header>
                        <p>

                            Jurusan Sistem Informasi, Fakultas Teknologi Informasi, Institut Teknologi Sepuluh Nopember ini berada di Sukolilo, Surabaya Utara. Dalam perjalanannya sampai saat ini, banyak yang telah dilalui dan bisa dituliskan sebagai penanda kemajuan SI | FTIF dengan tinta emas, seperti

                            <br/>Februari 2003, Mendapatkan hibah kompetisi SP4 dari DIKTI Kementerian Pendidikan Nasional RI
                            <br/>September 2005, Pertama kali meluluskan sarjana sistem informasi.
                            <br/>Agustus 2006, Mendapatkan akreditasi nasional dari BANPT dengan predikat A (sangat baik)
                            <br/>Nopember 2006, Memperoleh peringkat pertama dalam ITS Award katagori C (jurusan baru sengan jumlah mahasiswa di bawah 300 orang)
                            <br/>Maret 2007, Pertama kalinya mencetak majalah komunitas sistem informasi GengSi.
                            <br/>Nopember 2007, Memperoleh penghargaan khusus jurusan produktifitas terbaik dalam ITS Award.
                            <br/>Februari 2008, Peluncuran logo baru SI | FTIF
                            <br/>Agustus 2008, Pertama kalinya mencetak Si Ways, sebuah rekaman jalan-jalan yang dipilih untuk mengembangkan jurusan.
                            <br/>September 2008, Pertama kalinya joint research dengan perguruan tinggi luar negeri (Kumamoto University) yang diperoleh melalui program kerjasama ITS – JICA
                            <br/>Nopember 2008, Menerbitkan pertama kalinya Jurnal Sisfo (sistem informasi)
                            <br/>Desember 2008, Menyelenggarakan pertama kalinya seminar tingkat nasional dengan nama sesindo (seminar sistem informasi Indonesia)
                            <br/>Januari 2009, Pertama kalinya membukukan laporan pencapaian kinerja jurusan sebagai pertanggung jawaban ketua dan seketaris jurusan kepada stakeholder.
                            <br/>Maret 2009, Pertama kallinya menyelenggarakan recruitment (pegawai dosen dan karyawan) dengan menggunakan sistem on job training dan pembinaan.
                            <br/>Mei 2009, Menyiarkan pertama kalinya SITV (sistem informasi televisi)
                            <br/>Juni 2009, Mendirikan rintisan magister (S2) sistem informasi/teknologi informasi yang bekerjasama dengan program pasca sarjana teknik informatika FTIF ITS.
                            <br/>Nopember 2009, Memperoleh penghargaan khusus jurusan dengan kerjasama terbanyak dalam ITS Award.
                            <br/>Maret 2010, Terbentuknya forum orang tua mahasiswa sistem informasi (FORMASI)
                            <br/>Maret 2010, Pertama kalinya diselenggarakan parent gathering. Ajang unjuk karya mahasiswa dihadapan para orang tua/walinya.

                        </p>
                    </section>
                </div>

            </section>

            <!-- Footer -->
            <footer id="footer">


                <ul class="copyright">
                    <li><center>&copy; 2015 Institut Teknologi Sepuluh Nopember, Fakultas Teknologi Informasi, Jurusan Sistem Informasi</center></li>
                </ul>

            </footer>

    </body>
</html>