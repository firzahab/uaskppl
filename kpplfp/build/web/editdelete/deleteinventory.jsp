<%-- 
    Document   : deleteinventory
    Created on : Dec 26, 2015, 4:17:00 PM
    Author     : Niswati Pusparinda
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>

<html>
    <head>
        <title>Daftar Inventory</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.dropotron.min.js"></script>
        <script src="js/jquery.scrolly.min.js"></script>
        <script src="js/jquery.scrollgress.min.js"></script>
        <script src="js/skel.min.js"></script>
        <script src="js/skel-layers.min.js"></script>
        <script src="js/init.js"></script>
        <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
        <link rel="stylesheet" href="css/style-noscript.css" />
        </noscript>
        <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
        <!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->

    </head>

    <body>


        <%
            if (session.getAttribute("username") != null) {

                String id_barang = request.getParameter("u");

                try {
                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                    Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/kppl",
                            "root", "");
                    Statement smt = con.createStatement();
                    smt.executeUpdate("UPDATE inventory set deleted=1 where id_barang= '" + id_barang + "'");
                    response.sendRedirect("../cekinventoryadmin.jsp");
                } catch (Exception e1) {
                }
            } else {
        %>

        <section class="wrapper style4 container">

            <div class="content">
                <center><label class="control-label"> Anda Belum Login, Silahkan Login Terlebih Dahulu <a href="../loginadmin.jsp">Disini !</a></label> </center>
            </div>
        </section>
        <%
            }
        %>

    </body>
</html> 