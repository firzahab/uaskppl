
<!DOCTYPE HTML>

<!--
        Twenty by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->



<html>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%@page import="java.sql.*"%>
    <%@page import="java.io.*"%>
    <%@page import="java.sql.ResultSet"%>
    <%@page import="java.sql.Statement"%>
    <%@page import="java.sql.Connection"%>
    <%

    //deklarasi url database
        String url = "jdbc:mysql://localhost:3306/kppl";
        Connection con = null;
        Statement stat = null;
        ResultSet rs = null;

    //load jdbc driver
        Class.forName("com.mysql.jdbc.Driver");

        con = DriverManager.getConnection(url, "root", "");

        String id = request.getParameter("u");
        String submit = request.getParameter("submit");

        if (submit != null) {
            String nama_pelapor = request.getParameter("nama_pelapor");
            String nrp = request.getParameter("nrp");
            String nama_aset = request.getParameter("nama_aset");
            String kode_aset = request.getParameter("kode_aset");

            String ket_lapor = request.getParameter("ket_lapor");
            String statuslapor = request.getParameter("statuslapor");

            Statement smt = con.createStatement();
            smt.executeUpdate("UPDATE formpelaporan set nama_pelapor='" + nama_pelapor + "',nrp='" + nrp + "', nama_aset='" + nama_aset + "', kode_aset='" + kode_aset + "', ket_lapor='" + ket_lapor + "', statuslapor='" + statuslapor + "' where id_pelaporan='" + id + "'");
            response.sendRedirect("cekstatusadmin.jsp");

        }
        stat = con.createStatement();

    //membuat query
        String query = "select * from formpelaporan where id_pelaporan='" + id + "'";

        rs = stat.executeQuery(query);
        while (rs.next()) {
    %>
    <a href="indexform2.jsp"></a>
    <head>
        <title>Form Pelaporan Asset Laboratorium MSI</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
        <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
        <link rel="stylesheet" href="css/style-noscript.css" />
        </noscript>

        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.dropotron.min.js"></script>
        <script src="js/jquery.scrolly.min.js"></script>
        <script src="js/jquery.scrollgress.min.js"></script>
        <script src="js/skel.min.js"></script>
        <script src="js/skel-layers.min.js"></script>
        <script src="js/init.js"></script>


        <script src="http://d2g9qbzl5h49rh.cloudfront.net/static/prototype.forms.js" type="text/javascript"></script>
        <script src="http://d2g9qbzl5h49rh.cloudfront.net/static/jotform.forms.js?3.2.7057" type="text/javascript"></script>
        <script type="text/javascript">
            JotForm.init(function () {
                setTimeout(function () {
                    $('input_22').hint('ex: myname@example.com');
                }, 20);
                JotForm.calendarMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                JotForm.calendarDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
                JotForm.calendarOther = {"today": "Today"};
                JotForm.setCalendar("15", false);
                JotForm.onSubmissionError = "jumpToSubmit";
            });
        </script>
        <link href="http://d2g9qbzl5h49rh.cloudfront.net/static/formCss.css?3.2.7057" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="http://d2g9qbzl5h49rh.cloudfront.net/css/styles/nova.css?3.2.7057" />
        <link type="text/css" media="print" rel="stylesheet" href="http://d2g9qbzl5h49rh.cloudfront.net/css/printForm.css?3.2.7057" />
        <style type="text/css">
            .form-label-left{
                width:150px !important;
            }
            .form-line{
                padding-top:12px;
                padding-bottom:12px;
            }
            .form-label-right{
                width:150px !important;
            }
            .form-all{
                width:650px;
                color:#555 !important;
                font-family:'Arial Black';
                font-size:14px;
            }
            .form-radio-item label, .form-checkbox-item label, .form-grading-label, .form-header{
                color: #555;
            }

        </style>

        <style type="text/css" id="form-designer-style">
            /* Injected CSS Code */
            @import "https://fonts.googleapis.com/css?family=Chewy:light,lightitalic,normal,italic,bold,bolditalic";
            @import "//www.jotform.com/themes/css/buttons/form-submit-button-leather_black.css";
            .form-all {
                font-family: "Arial #000000", sans-serif;
            }
            .form-all {
                width: 650px;
            }
            .form-label-left,
            .form-label-right {
                width: 150px;
            }
            .form-label {
                white-space: normal;
            }
            .form-label.form-label-auto {
                display: inline-block;
                float: left;
                text-align: left;
                width: 150px;
            }
            .form-label-left {
                display: inline-block;
                white-space: normal;
                float: left;
                text-align: left;
            }
            .form-label-right {
                display: inline-block;
                white-space: normal;
                float: left;
                text-align: right;
            }
            .form-label-top {
                white-space: normal;
                display: block;
                float: none;
                text-align: left;
            }
            .form-all {
                font-size: 14px;
            }
            .form-label {
                font-weight: bold;
            }
            .form-checkbox-item label,
            .form-radio-item label {
                font-weight: normal;
            }
            .supernova {
                background-color: none;
                background-color: #ffffff;
            }
            .supernova body {
                background-color: transparent;
            }
            /*
            @width30: (unit(@formWidth, px) + 60px);
            @width60: (unit(@formWidth, px)+ 120px);
            @width90: (unit(@formWidth, px)+ 180px);
            */
            /* | */
            /* | */
            /* | */
            @media screen and (max-width: 480px) {
                .jotform-form {
                    padding: 10px 0;
                }
            }
            /* | */
            /* | */
            @media screen and (min-width: 480px) and (max-width: 768px) {
                .jotform-form {
                    padding: 30px 0;
                }
            }
            /* | */
            /* | */
            @media screen and (min-width: 768px) and (max-width: 1024px) {
                .jotform-form {
                    padding: 60px 0;
                }
            }
            /* | */
            /* | */
            @media screen and (min-width: 1024px) {
                .jotform-form {
                    padding: 90px 0;
                }
            }
            /* | */
            .supernova .form-all,
            .form-all {
                background-color: none;
                border: 1px solid transparent;
            }
            .form-all {
                color: #000000;
            }
            .form-header-group .form-header {
                color: #000000;
            }
            .form-header-group .form-subHeader {
                color: #1a1a1a;
            }
            .form-sub-label {
                color: #1a1a1a;
            }
            .form-label-top,
            .form-label-left,
            .form-label-right,
            .form-html {
                color: #000000;
            }
            .form-checkbox-item label,
            .form-radio-item label {
                color: #1a1a1a;
            }
            .form-line.form-line-active {
                -webkit-transition-property: all;
                -moz-transition-property: all;
                -ms-transition-property: all;
                -o-transition-property: all;
                transition-property: all;
                -webkit-transition-duration: 0.3s;
                -moz-transition-duration: 0.3s;
                -ms-transition-duration: 0.3s;
                -o-transition-duration: 0.3s;
                transition-duration: 0.3s;
                -webkit-transition-timing-function: ease;
                -moz-transition-timing-function: ease;
                -ms-transition-timing-function: ease;
                -o-transition-timing-function: ease;
                transition-timing-function: ease;
                background-color: inherit;
            }
            /* Ã¶mer */
            .form-radio-item,
            .form-checkbox-item {
                padding-bottom: 0px !important;
            }
            .form-radio-item:last-child,
            .form-checkbox-item:last-child {
                padding-bottom: 0;
            }
            /* Ã¶mer */
            [data-type="control_radio"] .form-input,
            [data-type="control_checkbox"] .form-input,
            [data-type="control_radio"] .form-input-wide,
            [data-type="control_checkbox"] .form-input-wide {
                width: 100%;
                max-width: 328px;
            }
            .form-radio-item,
            .form-checkbox-item {
                width: 100%;
                max-width: 328px;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }
            .form-textbox.form-radio-other-input,
            .form-textbox.form-checkbox-other-input {
                width: 80%;
                margin-left: 3%;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }
            .form-multiple-column {
                width: 100%;
            }
            .form-multiple-column .form-radio-item,
            .form-multiple-column .form-checkbox-item {
                width: 10%;
            }
            .form-multiple-column[data-columncount="1"] .form-radio-item,
            .form-multiple-column[data-columncount="1"] .form-checkbox-item {
                width: 100%;
            }
            .form-multiple-column[data-columncount="2"] .form-radio-item,
            .form-multiple-column[data-columncount="2"] .form-checkbox-item {
                width: 50%;
            }
            .form-multiple-column[data-columncount="3"] .form-radio-item,
            .form-multiple-column[data-columncount="3"] .form-checkbox-item {
                width: 33.33333333%;
            }
            .form-multiple-column[data-columncount="4"] .form-radio-item,
            .form-multiple-column[data-columncount="4"] .form-checkbox-item {
                width: 25%;
            }
            .form-multiple-column[data-columncount="5"] .form-radio-item,
            .form-multiple-column[data-columncount="5"] .form-checkbox-item {
                width: 20%;
            }
            .form-multiple-column[data-columncount="6"] .form-radio-item,
            .form-multiple-column[data-columncount="6"] .form-checkbox-item {
                width: 16.66666667%;
            }
            .form-multiple-column[data-columncount="7"] .form-radio-item,
            .form-multiple-column[data-columncount="7"] .form-checkbox-item {
                width: 14.28571429%;
            }
            .form-multiple-column[data-columncount="8"] .form-radio-item,
            .form-multiple-column[data-columncount="8"] .form-checkbox-item {
                width: 12.5%;
            }
            .form-multiple-column[data-columncount="9"] .form-radio-item,
            .form-multiple-column[data-columncount="9"] .form-checkbox-item {
                width: 11.11111111%;
            }
            .form-single-column .form-checkbox-item,
            .form-single-column .form-radio-item {
                width: 100%;
            }
            .supernova {
                height: 100%;
                background-repeat: no-repeat;
                background-attachment: scroll;
                background-position: center top;
                background-repeat: repeat;
                background-attachment: fixed;
                background-size: auto;
                background-size: cover;
            }
            .supernova {
                background-image: none;
            }
            #stage {
                background-image: none;
            }
            /* | */
            .form-all {
                background-repeat: no-repeat;
                background-attachment: scroll;
                background-position: center top;
                background-repeat: repeat;
            }
            .form-header-group {
                background-repeat: no-repeat;
                background-attachment: scroll;
                background-position: center top;
            }
            .form-line {
                margin-top: 10px;
                margin-bottom: 10px;
            }
            .form-line {
                padding: 1px 36px;
            }
            .form-all .form-submit-button,
            .form-all .form-submit-reset,
            .form-all .form-submit-print {
                -webkit-border-radius: 6px;
                -moz-border-radius: 6px;
                border-radius: 6px;
            }
            .form-all .form-sub-label {
                margin-left: 3px;
            }
            .form-all {
                -webkit-border-radius: 20px;
                -moz-border-radius: 20px;
                border-radius: 20px;
            }
            .form-section:first-child {
                -webkit-border-radius: 20px 20px 0 0;
                -moz-border-radius: 20px 20px 0 0;
                border-radius: 20px 20px 0 0;
            }
            .form-section:last-child {
                -webkit-border-radius: 0 0 20px 20px;
                -moz-border-radius: 0 0 20px 20px;
                border-radius: 0 0 20px 20px;
            }
            .form-all .qq-upload-button,
            .form-all .form-submit-button,
            .form-all .form-submit-reset,
            .form-all .form-submit-print {
                height: 54px;
                width: 92px;
                font-size: 1.15em;
                padding: 12px 18px;
                font-family: "Chewy", sans-serif;
                font-size: 21px;
                font-weight: normal;
                border: none;
                border-width: 2px !important;
                border-style: dashed !important;
                border-color: #fffbea !important;
            }
            .form-all .form-submit-print {
                margin-left: 0 !important;
                margin-right: 0 !important;
            }
            .form-all .qq-upload-button,
            .form-all .form-submit-button,
            .form-all .form-submit-reset,
            .form-all .form-submit-print {
                color:  !important;
            }
            .form-all .form-pagebreak-back,
            .form-all .form-pagebreak-next {
                font-size: 1em;
                padding: 9px 15px;
                font-family: "Arial #000000", sans-serif;
                font-size: 14px;
                font-weight: normal;
            }
            /*
            &amp; when ( @buttonFontType = google ) {
                    @import (css) "@{buttonFontLink}";
            }
            */
            h2.form-header {
                line-height: 1.618em;
            }
            h2 ~ .form-subHeader {
                line-height: 1.5em;
            }
            .form-header-group {
                text-align: center;
            }
            .form-line {
                zoom: 1;
            }
            .form-line:before,
            .form-line:after {
                display: table;
                content: '';
                line-height: 0;
            }
            .form-line:after {
                clear: both;
            }
            .form-sub-label-container {
                margin-right: 0;
                float: left;
                white-space: nowrap;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }
            .form-sub-label-container .date-separate {
                visibility: hidden;
            }
            .form-captcha input,
            .form-spinner input {
                width: 330px;
            }
            .form-textbox,
            .form-textarea {
                width: 100%;
                max-width: 330px;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }
            .form-input,
            .form-address-table,
            .form-matrix-table {
                width: 100%;
                max-width: 330px;
            }
            .form-radio-item,
            .form-checkbox-item {
                width: 100%;
                max-width: 330px;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }
            .form-textbox.form-radio-other-input,
            .form-textbox.form-checkbox-other-input {
                width: 80%;
                margin-left: 3%;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }
            .form-multiple-column {
                width: 100%;
            }
            .form-multiple-column .form-radio-item,
            .form-multiple-column .form-checkbox-item {
                width: 10%;
            }
            .form-multiple-column[data-columncount="1"] .form-radio-item,
            .form-multiple-column[data-columncount="1"] .form-checkbox-item {
                width: 100%;
            }
            .form-multiple-column[data-columncount="2"] .form-radio-item,
            .form-multiple-column[data-columncount="2"] .form-checkbox-item {
                width: 50%;
            }
            .form-multiple-column[data-columncount="3"] .form-radio-item,
            .form-multiple-column[data-columncount="3"] .form-checkbox-item {
                width: 33.33333333%;
            }
            .form-multiple-column[data-columncount="4"] .form-radio-item,
            .form-multiple-column[data-columncount="4"] .form-checkbox-item {
                width: 25%;
            }
            .form-multiple-column[data-columncount="5"] .form-radio-item,
            .form-multiple-column[data-columncount="5"] .form-checkbox-item {
                width: 20%;
            }
            .form-multiple-column[data-columncount="6"] .form-radio-item,
            .form-multiple-column[data-columncount="6"] .form-checkbox-item {
                width: 16.66666667%;
            }
            .form-multiple-column[data-columncount="7"] .form-radio-item,
            .form-multiple-column[data-columncount="7"] .form-checkbox-item {
                width: 14.28571429%;
            }
            .form-multiple-column[data-columncount="8"] .form-radio-item,
            .form-multiple-column[data-columncount="8"] .form-checkbox-item {
                width: 12.5%;
            }
            .form-multiple-column[data-columncount="9"] .form-radio-item,
            .form-multiple-column[data-columncount="9"] .form-checkbox-item {
                width: 11.11111111%;
            }
            [data-type="control_dropdown"] .form-dropdown {
                width: 100% !important;
                max-width: 330px;
            }
            [data-type="control_fullname"] .form-sub-label-container {
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                width: 48%;
            }
            [data-type="control_fullname"] .form-sub-label-container:first-child {
                margin-right: 4%;
            }
            [data-type="control_phone"] .form-sub-label-container {
                width: 65%;
            }
            [data-type="control_phone"] .form-sub-label-container:first-child {
                width: 32.5%;
                margin-right: 2.5%;
            }
            [data-type="control_birthdate"] .form-sub-label-container {
                width: 22%;
                margin-right: 3%;
            }
            [data-type="control_birthdate"] .form-sub-label-container:first-child {
                width: 50%;
            }
            [data-type="control_birthdate"] .form-sub-label-container:last-child {
                margin-right: 0;
            }
            [data-type="control_birthdate"] .form-sub-label-container .form-dropdown {
                width: 100%;
            }
            [data-type="control_time"] .form-sub-label-container {
                width: 37%;
                margin-right: 3%;
            }
            [data-type="control_time"] .form-sub-label-container:last-child {
                width: 20%;
                margin-right: 0;
            }
            [data-type="control_time"] .form-sub-label-container .form-dropdown {
                width: 100%;
            }
            [data-type="control_datetime"] .form-sub-label-container {
                width: 28%;
                margin-right: 4%;
            }
            [data-type="control_datetime"] .form-sub-label-container:last-child {
                width: 4%;
                margin-right: 0;
            }
            [data-type="control_datetime"].allowTime .form-sub-label-container {
                width: 12%;
                margin-right: 3%;
            }
            [data-type="control_datetime"].allowTime .form-sub-label-container:last-child {
                width: 4%;
                margin-right: 0;
            }
            [data-type="control_datetime"].allowTime span .form-sub-label-container:first-child {
                width: 3%;
            }
            [data-type="control_datetime"].allowTime span .form-sub-label-container:last-child {
                width: 12%;
                margin-right: 3%;
            }
            [data-type="control_datetime"].allowTime .form-dropdown {
                width: 100%;
            }
            [data-type="control_payment"] .form-sub-label-container {
                width: auto;
            }
            [data-type="control_payment"] .form-sub-label-container .form-dropdown {
                width: 100%;
            }
            .form-address-table td .form-dropdown {
                width: 100%;
            }
            .form-address-table td .form-sub-label-container {
                width: 96%;
            }
            .form-address-table td:last-child .form-sub-label-container {
                margin-left: 4%;
            }
            .form-address-table td[colspan="2"] .form-sub-label-container {
                width: 100%;
                margin: 0;
            }
            /*.form-dropdown,
            .form-radio-item,
            .form-checkbox-item,
            .form-radio-other-input,
            .form-checkbox-other-input,*/
            .form-captcha input,
            .form-spinner input,
            .form-error-message {
                padding: 4px 3px 2px 3px;
            }
            .form-header-group {
                font-family: "Chewy", sans-serif;
            }
            .form-section {
                padding: 0px 0px 0px 0px;
            }
            .form-header-group {
                margin: 12px 36px 12px 36px;
            }
            .form-header-group {
                padding: 24px 0px 24px 0px;
            }
            .form-header-group .form-header,
            .form-header-group .form-subHeader {
                color: #1c709e;
            }
            .form-header-group {
                background-color: 
            }
            .form-textbox,
            .form-textarea {
                padding: 4px 3px 2px 3px;
            }
            .form-textbox,
            .form-textarea,
            .form-radio-other-input,
            .form-checkbox-other-input,
            .form-captcha input,
            .form-spinner input {
                background-color: inherit;
            }
            .form-textbox,
            .form-textarea {
                width: 100%;
                max-width: 330px;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
            }
            [data-type="control_textbox"] .form-input,
            [data-type="control_textarea"] .form-input,
            [data-type="control_fullname"] .form-input,
            [data-type="control_phone"] .form-input,
            [data-type="control_datetime"] .form-input,
            [data-type="control_address"] .form-input,
            [data-type="control_email"] .form-input,
            [data-type="control_passwordbox"] .form-input,
            [data-type="control_autocomp"] .form-input,
            [data-type="control_textbox"] .form-input-wide,
            [data-type="control_textarea"] .form-input-wide,
            [data-type="control_fullname"] .form-input-wide,
            [data-type="control_phone"] .form-input-wide,
            [data-type="control_datetime"] .form-input-wide,
            [data-type="control_address"] .form-input-wide,
            [data-type="control_email"] .form-input-wide,
            [data-type="control_passwordbox"] .form-input-wide,
            [data-type="control_autocomp"] .form-input-wide {
                width: 100%;
                max-width: 330px;
            }
            [data-type="control_fullname"] .form-sub-label-container {
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                width: 48%;
            }
            [data-type="control_fullname"] .form-sub-label-container:first-child {
                margin-right: 4%;
            }
            [data-type="control_phone"] .form-sub-label-container {
                width: 65%;
            }
            [data-type="control_phone"] .form-sub-label-container:first-child {
                width: 32.5%;
                margin-right: 2.5%;
            }
            [data-type="control_phone"] .form-sub-label-container .date-separate {
                visibility: hidden;
            }
            [data-type="control_datetime"] .form-sub-label-container {
                width: 28%;
                margin-right: 4%;
            }
            [data-type="control_datetime"] .form-sub-label-container:last-child {
                width: 4%;
                margin-right: 0;
            }
            [data-type="control_datetime"] .form-sub-label-container .date-separate {
                visibility: hidden;
            }
            [data-type="control_datetime"].allowTime .form-sub-label-container {
                width: 12%;
                margin-right: 3%;
            }
            [data-type="control_datetime"].allowTime .form-sub-label-container:last-child {
                width: 4%;
                margin-right: 0;
            }
            [data-type="control_datetime"].allowTime span .form-sub-label-container:first-child {
                width: 3%;
            }
            [data-type="control_datetime"].allowTime span .form-sub-label-container:last-child {
                width: 12%;
                margin-right: 3%;
            }
            [data-type="control_datetime"].allowTime .form-dropdown {
                width: 100%;
            }
            .form-matrix-table {
                width: 100%;
                max-width: 330px;
            }
            .form-address-table {
                width: 100%;
                max-width: 330px;
            }
            .form-address-table td .form-dropdown {
                width: 100%;
            }
            .form-address-table td .form-sub-label-container {
                width: 96%;
            }
            .form-address-table td:last-child .form-sub-label-container {
                margin-left: 4%;
            }
            .form-address-table td[colspan="2"] .form-sub-label-container {
                width: 100%;
                margin: 0;
            }
            [data-type="control_dropdown"] .form-input,
            [data-type="control_birthdate"] .form-input,
            [data-type="control_time"] .form-input,
            [data-type="control_dropdown"] .form-input-wide,
            [data-type="control_birthdate"] .form-input-wide,
            [data-type="control_time"] .form-input-wide {
                width: 100%;
                max-width: 330px;
            }
            [data-type="control_dropdown"] .form-dropdown {
                width: 100% !important;
                max-width: 330px;
            }
            [data-type="control_birthdate"] .form-sub-label-container {
                width: 22%;
                margin-right: 3%;
            }
            [data-type="control_birthdate"] .form-sub-label-container:first-child {
                width: 50%;
            }
            [data-type="control_birthdate"] .form-sub-label-container:last-child {
                margin-right: 0;
            }
            [data-type="control_birthdate"] .form-sub-label-container .form-dropdown {
                width: 100%;
            }
            [data-type="control_time"] .form-sub-label-container {
                width: 37%;
                margin-right: 3%;
            }
            [data-type="control_time"] .form-sub-label-container:last-child {
                width: 20%;
                margin-right: 0;
            }
            [data-type="control_time"] .form-sub-label-container .form-dropdown {
                width: 100%;
            }
            .form-buttons-wrapper {
                margin-left: 0 !important;
                text-align: center !important;
            }
            .form-label {
                margin-right: 0px;
                margin-bottom: 0;
            }
            .form-label {
                font-family: "Arial #000000", sans-serif;
            }
            li[data-type="control_image"] div {
                text-align: left;
            }
            li[data-type="control_image"] img {
                border: none;
                border-width: 0px !important;
                border-style: solid !important;
                border-color: false !important;
            }
            .form-line-column {
                width: auto;
            }
            .form-line-error {
                overflow: hidden;
                -webkit-transition-property: none;
                -moz-transition-property: none;
                -ms-transition-property: none;
                -o-transition-property: none;
                transition-property: none;
                -webkit-transition-duration: 0.3s;
                -moz-transition-duration: 0.3s;
                -ms-transition-duration: 0.3s;
                -o-transition-duration: 0.3s;
                transition-duration: 0.3s;
                -webkit-transition-timing-function: ease;
                -moz-transition-timing-function: ease;
                -ms-transition-timing-function: ease;
                -o-transition-timing-function: ease;
                transition-timing-function: ease;
                background-color: inherit;
            }
            .form-line-error .form-error-message {
                background-color: #ff3200;
                clear: both;
                float: none;
            }
            .form-line-error .form-error-message .form-error-arrow {
                border-bottom-color: #ff3200;
            }
            .form-line-error input:not(#coupon-input),
            .form-line-error textarea,
            .form-line-error .form-validation-error {
                border: 1px solid #ff3200;
                -webkit-box-shadow: 0 0 3px #ff3200;
                -moz-box-shadow: 0 0 3px #ff3200;
                box-shadow: 0 0 3px #ff3200;
            }
            .ie-8 .form-all {
                margin-top: auto;
                margin-top: initial;
            }
            .ie-8 .form-all:before {
                display: none;
            }
            /* | */
            @media screen and (max-width: 480px), screen and (max-device-width: 768px) and (orientation: portrait), screen and (max-device-width: 415px) and (orientation: landscape) {
                .jotform-form {
                    padding: 0;
                }
                .form-all {
                    border: 0;
                    width: 100%;
                    max-width: initial;
                }
                .form-sub-label-container {
                    width: 100%;
                    margin: 0;
                }
                .form-input {
                    width: 100%;
                }
                .form-label {
                    width: 100%!important;
                }
                .form-line {
                    padding: 2% 5%;
                    -moz-box-sizing: border-box;
                    -webkit-box-sizing: border-box;
                    box-sizing: border-box;
                }
                input[type=text],
                input[type=email],
                input[type=tel],
                textarea {
                    width: 100%;
                    -moz-box-sizing: border-box;
                    -webkit-box-sizing: border-box;
                    box-sizing: border-box;
                    max-width: initial !important;
                }
                .form-input,
                .form-input-wide,
                .form-textarea,
                .form-textbox,
                .form-dropdown {
                    max-width: initial !important;
                }
                div.form-header-group {
                    padding: 24px 0px !important;
                    margin: 0 12px 2% !important;
                    margin-left: 5% !important;
                    margin-right: 5% !important;
                    -moz-box-sizing: border-box;
                    -webkit-box-sizing: border-box;
                    box-sizing: border-box;
                }
                [data-type="control_button"] {
                    margin-bottom: 0 !important;
                }
                .form-buttons-wrapper {
                    margin: 0!important;
                }
                .form-buttons-wrapper button {
                    width: 100%;
                }
                table {
                    width: 100%!important;
                    max-width: initial !important;
                }
                table td + td {
                    padding-left: 3%;
                }
                .form-checkbox-item input,
                .form-radio-item input {
                    width: auto;
                }
                .form-collapse-table {
                    margin: 0 5%;
                }
            }
            /* | */

            /*__INSPECT_SEPERATOR__*/

            /* Injected CSS Code */
        </style>

        <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
        <!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
    </head>
    <body class="no-sidebar">

        <!-- Header -->
        <header id="header" class="skel-layers-fixed">
            <h1 id="logo"><a href="index.html">ASSET MANAGEMENT LABORATORIUM MANAJEMEN SISTEM INFORMASI</a></h1>
            <nav id="nav">
                <ul>
                    <li class="current"><a href="indexadmin.jsp">Home</a></li>
                    <li class="submenu">
                        <a href="">Menu</a>
                        <ul>
                            <!--<li><a href="left-sidebar.html">Left Sidebar</a></li>  Left Sidebar -->
                            <!--<li><a href="right-sidebar.html">Right Sidebar</a></li> Right Sidebar -->
                            <li><a href="cekinventoryadmin.jsp">Daftar Aset</a></li>
                            <li><a href="cekstatusadmin.jsp">Cek Status Pelaporan</a></li>
                            <li><a href="record.jsp">Record Activity</a></li>


                        </ul>
                    </li>
                    <%
                        if ((session.getAttribute("username") == null)) {
                    %>
                    <li><a href="loginadmin.jsp" class="button special">Login</a></li>
                        <%} else {
                        %>
                    <li><a href="./loginadmin/logoutadmin.jsp" class="button special">Logout</a></li>
                        <%
                            }
                        %>
                </ul>
            </nav>
        </header>
        <%
            if (session.getAttribute("username") != null) {
        %>
        <!-- Main -->
        <article id="main">

            <header class="special container">
                <span class="icon fa-mobile"></span>
                <h2><strong>Formulir Pelaporan Aset Laboratorium MSI</strong></h2>
                <p>Isi formulir sesuai dengan kebutuhan anda!!!</p>
            </header>

            <!-- One -->
            <section class="wrapper style4 container">

                <!-- Content -->
                <div class="content">

                    <form class="jotform-form" action="" method="post" name="form_51244451398457" id="51244451398457" accept-charset="utf-8">
                        <input type="hidden" name="formID" value="51244451398457" />
                        <div class="form-all">
                            <ul class="form-section page-section">
                                <li class="form-line" data-type="control_text" id="id_13">
                                    <div id="cid_13" class="form-input-wide">
                                        <div id="text_13" class="form-html">
                                            <p>Identitas pelapor :</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="form-line jf-required" data-type="control_textbox" id="id_4">
                                    <label class="form-label form-label-left form-label-auto" id="label_4" for="input_4">
                                        Nama Lengkap
                                        <span class="form-required">
                                            *
                                        </span>
                                    </label>
                                    <div id="cid_4" class="form-input jf-required">
                                        <input value ="<%=rs.getString(2)%> " type="text" class=" form-textbox validate[required]" data-type="input-textbox" id="input_4" name="nama_pelapor" size="20" />
                                    </div>
                                </li>
                                <li class="form-line jf-required" data-type="control_textbox" id="id_5">
                                    <label class="form-label form-label-left form-label-auto" id="label_5" for="input_5">
                                        NRP
                                        <span class="form-required">
                                            *
                                        </span>
                                    </label>
                                    <div id="cid_5" class="form-input jf-required">
                                        <input value ="<%=rs.getString(3)%>" type="text" class=" form-textbox validate[required]" data-type="input-textbox" id="input_5" name="nrp" size="20" />
                                    </div>
                                </li>

                                <li class="form-line jf-required" data-type="control_dropdown" id="id_25">
                                    <label class="form-label form-label-left form-label-auto" id="label_25" for="input_25">
                                        Nama Asset
                                        <span class="form-required">
                                            *
                                        </span>
                                    </label>
                                    <div id="cid_25" class="form-input jf-required">
                                        <select class="form-dropdown validate[required]" style="width:150px" id="input_25" name="nama_aset">
                                            <option value="">  </option>
                                            <option value="Monitor PC"> Monitor PC </option>
                                            <option value="Printer"> Printer </option>
                                            <option value="Layar LCD"> Layar LCD </option>
                                            <option value="LCD Proyektor"> LCD Proyektor </option>
                                            <option value="FlashDisk "> FlashDisk </option>
                                            <option value="HardDisk"> HardDisk </option>
                                            <option value="Speaker"> Speaker </option>
                                            <option value="Mic"> Mic </option>
                                            <option value="CPU"> CPU </option>
                                            <option value="Mouse"> Mouse </option>
                                            <option value="Keyboard"> Keyboard </option>
                                            <option value="Spidol"> Spidol </option>
                                            <option value="Tinta Spidol"> Tinta Spidol </option>
                                            <option value="Bolpoin"> Bolpoin </option>
                                            <option value="Penghapus"> Penghapus </option>
                                        </select>
                                    </div>
                                </li> 

                                <li class="form-line jf-required" data-type="control_phone" id="id_28">
                                    <label class="form-label form-label-left form-label-auto" id="label_28" for="input_28">
                                        Kode Asset
                                        <span class="form-required">
                                            *
                                        </span>
                                    </label>
                                    <div id="cid_28" class="form-input jf-required">
                                        <span class="form-sub-label-container" style="vertical-align: top">
                                            <input value="<%=rs.getString(5)%>" class="form-textbox validate[required]" type="tel" name="kode_aset" id="input_28_area" size="3">
                                            <span class="phone-separate">
                                            </span>
                                            <label class="form-sub-label" for="input_28_area" id="sublabel_area" style="min-height: 12px;"> Masukkan kode asset sesuai yang tertera di barang </label>
                                        </span>
                                    </div>
                                </li>

                                <li class="form-line jf-required" data-type="control_textbox" id="id_31">
                                    <label class="form-label form-label-left form-label-auto" id="label_31" for="input_31">
                                        Tanggal Pelaporan
                                        <span class="form-required">
                                            *
                                        </span> 
                                    </label>
                                    <div id="cid_31" class="form-input jf-required">
                                        <input value ="<%=rs.getString(6)%>" type="date" class=" form-textbox validate[required]" data-type="input-textbox" id="input_31" name="eventdate" size="20" value="" placeholder="YYYY-MM-DD  eg.2015-11-02" />
                                    </div>
                                </li>

                                <li class="form-line" data-type="control_text" id="id_17">
                                    <div id="cid_17" class="form-input-wide">
                                        <div id="text_17" class="form-html">
                                            <p>Pelaporan ini dilakukan karena: </p>
                                        </div>
                                    </div>
                                </li>

                                <li class="form-line jf-required" data-type="control_textarea" id="id_18">
                                    <label class="form-label form-label-left form-label-auto" id="label_18" for="input_18">
                                        Keterangan Pelaporan
                                        <span class="form-required">
                                            *
                                        </span>
                                    </label>
                                    <div id="cid_18" class="form-input jf-required">
                                        <textarea id="input_18" class="form-textarea validate[required]" name="ket_lapor" cols="40" rows="6"><%=rs.getString(7)%></textarea>
                                    </div>
                                </li> 


                                <li class="form-line jf-required" data-type="control_dropdown" id="id_25">
                                    <label class="form-label form-label-left form-label-auto" id="label_25" for="input_25">
                                        Status Pelaporan
                                        <span class="form-required">
                                            *
                                        </span>
                                    </label>
                                    <div id="cid_25" class="form-input jf-required">
                                        <select class="form-dropdown validate[required]" style="width:150px" id="input_25" name="statuslapor">
                                            <option value="">  </option>
                                            <option value="Menunggu"> Menunggu </option>
                                            <option value="Sedang diproses"> Sedang diproses </option>
                                            <option value="Selesai diproses"> Selesai diproses </option>
                                            <option value="Tidak diproses"> Tidak diproses </option>

                                        </select>
                                    </div>
                                </li> 


                                <li class="form-line" data-type="control_button" id="id_2">
                                    <div id="cid_2" class="form-input-wide">
                                        <div style="margin-left:156px" class="form-buttons-wrapper">
                                            <button id="input_2" type="submit" name="submit" class="form-submit-button">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </li>
                                <li style="display:none">
                                    Should be Empty:
                                    <input value ="<%=rs.getString(3)%>" type="text" name="website" value="" />
                                </li>
                            </ul>
                        </div>
                        <input type="hidden" id="simple_spc" name="simple_spc" value="51244451398457" />
                        <script type="text/javascript">
                            document.getElementById("si" + "mple" + "_spc").value = "51244451398457-51244451398457";
                        </script>
                    </form>
            </section>
            <%} else {
            %>

            <section class="wrapper style4 container">

                <div class="content">
                    <center><label class="control-label"> Anda Belum Login, Silahkan Login Terlebih Dahulu <a href="loginadmin.jsp">Disini !</a></label> </center>
                </div>
            </section>
            <%
                }
            %>

            <!-- Footer -->
            <footer id="footer">


                <ul class="copyright">
                    <li><center>&copy; 2015 Institut Teknologi Sepuluh Nopember, Fakultas Teknologi Informasi, Jurusan Sistem Informasi</center></li>
                </ul>

            </footer>

    </body>
</html>

<%
    }
    //menutup koneksi
// rs.close();
    //stat.close();
    //con.close();
%>