<!DOCTYPE HTML>

<!--
	Twenty by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->

<html>
<?php
include "connectcek.php";

?>

	<head>
		<title>Form Peminjaman Tempat</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/jquery.scrolly.min.js"></script>
		<script src="js/jquery.scrollgress.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-wide.css" />
			<link rel="stylesheet" href="css/style-noscript.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
		
	</head>
	
	<body class="no-sidebar">
	
		<!-- Header -->
			<header id="header" class="skel-layers-fixed">
				<h1 id="logo"><a href="index.html">Created by <span>Sarah Putri Ramadhani 5213100185</span></a></h1>
				<nav id="nav">
					<ul>
						<li class="current"><a href="index.html">Home</a></li>
						<li class="submenu">
							<a href="">Menu</a>
							<ul>
								<!--<li><a href="left-sidebar.html">Left Sidebar</a></li>  Left Sidebar -->
								<!--<li><a href="right-sidebar.html">Right Sidebar</a></li> Right Sidebar -->
								<li><a href="about.html">About JSI</a></li>
								<li><a href="tempat.html">Tempat JSI</a></li>
								<li><a href="jadwal.html">Jadwal Pemakaian Tempat</a></li>
								<li><a href="indexform2.php">Formulir Peminjaman Tempat</a></li> <!--No Sidebar -->
								<li><a href="cekstatus.php">Cek Status Peminjaman Tempat</a></li>

							</ul>
						</li>
						<li><a href="/formtempat/logout.php" class="button special">Logout</a></li>
					</ul>
				</nav>
			</header>

		<!-- Main -->

			<article id="main">

				<header class="special container">
					<span class="icon fa-mobile"></span>
					<h2><strong>Jadwal Tetap Penggunaan Tempat</strong></h2>
				</header>

				<!-- One -->
					<section class="wrapper style4 container2">

						<!-- Content -->
							<div class="content">

				<table id="tblData-table" border="3%" width="110%" class="order-table">
					<tr>
						<td width="10%"><b><center>Tempat</center></b></td>
						<td width="20%"><b><center>Hari</center></b></td>
						<td width="10%"><b><center>Pukul</center></b></td>
						<td width="10%"><b><center>Kegiatan</center></b></td>

					</tr>
					<tr>
						<td>TC-101</td>
						<td>Senin - kamis</td>
						<td>07.00 - 18.00</td>
						<td>Perkuliahan</td>
					</tr>
					
					<tr>
						<td>TC-102</td>
						<td>Senin - Jumat</td>
						<td>07.00 - 18.00</td>
						<td>Perkuliahan</td>
					</tr>
					
					<tr>
						<td>TC-103</td>
						<td>Senin - Jumat</td>
						<td>07.00 - 18.00</td>
						<td>Perkuliahan</td>
					</tr>
					
					<tr>
						<td>TC-104</td>
						<td>Senin - Jumat</td>
						<td>07.00 - 18.00</td>
						<td>Perkuliahan</td>
					</tr>
					
					<tr>
						<td>TC-105</td>
						<td>Senin - Jumat</td>
						<td>07.00 - 18.00</td>
						<td>Perkuliahan</td>
					</tr>
					
					<tr>
						<td>TC-106</td>
						<td>Senin - Jumat</td>
						<td>07.00 - 18.00</td>
						<td>Perkuliahan</td>
					</tr>
					
					<tr>
						<td>TC-107</td>
						<td>Senin - Jumat</td>
						<td>07.00 - 18.00</td>
						<td>Perkuliahan</td>
					</tr>
					
					<tr>
						<td>TC-108</td>
						<td>Senin - Jumat</td>
						<td>07.00 - 18.00</td>
						<td>Perkuliahan</td>
					</tr>
					<tr>
						<td>Plasa Mading</td>
						<td>Senin - Kamis</td>
						<td>07.00 - 18.00</td>
						<td>Perkuliahan</td>
					</tr>
					<tr>
						<td>Plasa Mading</td>
						<td>Jumat</td>
						<td>13.00 - 18.00</td>
						<td>Perkuliahan</td>
					</tr>
					
				</table>
			</div>
				

</body>
</html>