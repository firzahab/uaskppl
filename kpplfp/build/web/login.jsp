<!DOCTYPE HTML>

<!--
        Twenty by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->

<html>
    <?php
    include "connectcek.php";

    ?>

    <head>
        <title>Login dan Registrasi</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.dropotron.min.js"></script>
        <script src="js/jquery.scrolly.min.js"></script>
        <script src="js/jquery.scrollgress.min.js"></script>
        <script src="js/skel.min.js"></script>
        <script src="js/skel-layers.min.js"></script>
        <script src="js/init.js"></script>
        <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
        <link rel="stylesheet" href="css/style-noscript.css" />
        </noscript>
        <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
        <!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->

    </head>

    <body class="no-sidebar">

        <!-- Header -->
        <header id="header" class="skel-layers-fixed">
            <h1 id="logo"><a href="index.jsp">ASSET MANAGEMENT LABORATORIUM MANAJEMEN SISTEM INFORMASI</a></h1>
            <nav id="nav">
                <ul>
                    <li class="current"><a href="index.jsp">Home</a></li>
                    <li class="submenu">
                        <a href="">Menu</a>
                        <ul>
                            <!--<li><a href="left-sidebar.html">Left Sidebar</a></li>  Left Sidebar -->
                            <!--<li><a href="right-sidebar.html">Right Sidebar</a></li> Right Sidebar -->
                            <li><a href="about.jsp">Tentang JSI</a></li>
                            <li><a href="tempat.jsp">Daftar Aset</a></li>
                            <li><a href="indexform2.jsp">Formulir Pelaporan Aset</a></li> <!--No Sidebar -->
                            <li><a href="cekstatus.jsp">Cek Status Pelaporan</a></li>

                        </ul>
                    </li>

                </ul>
            </nav>
        </header>

        <!-- Main -->

        <article id="main">
            <header class="special container">
                <span class="icon fa-mobile"></span>
                <h2><strong>Silahkan Login dengan Akun yang Telah Terdaftar</strong></h2>
            </header>

            <!-- One -->
            <section class="wrapper style4 container2">

                <!-- Content -->
                <div class="container">


                    <div class="row">
                        <div class="span6">

                            <form class="form-horizontal well" action="login/login.jsp" method="post">
                                <fieldset>
                                    <H2>Log in</H2>
                                    <div class="control-group">
                                        <label class="control-label" for="user">Username</label>

                                        <div class="controls">
                                            <input type="text" name="user" id="user" placeholder="Username">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="Password">Password</label>

                                        <div class="controls">
                                            <input type="password" name="Password" id="Password" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="controls">
                                            <label class="checkbox">
                                                <input type="checkbox"> Remember Me
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <input type="submit" class="button special" value="Login" /> 
                                        <!--<button type="submit" class="btn btn-primary">Login</button>-->
                                        <!-- <button type="button" class="btn">Cancel</button>-->
                                    </div>
                                    <label class="control-label" for="pPassword"> Belum Terdaftar??<a href="regis.jsp">Daftar Disini</a></label>
                                </fieldset>
                            </form>
                        </div>
                    </div>





                    </body>
                    </html>