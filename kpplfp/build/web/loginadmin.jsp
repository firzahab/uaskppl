<!DOCTYPE HTML>

<!--
        Twenty by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->

<html>

    <head>
        <title>Login dan Registrasi</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.dropotron.min.js"></script>
        <script src="js/jquery.scrolly.min.js"></script>
        <script src="js/jquery.scrollgress.min.js"></script>
        <script src="js/skel.min.js"></script>
        <script src="js/skel-layers.min.js"></script>
        <script src="js/init.js"></script>
        <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
        <link rel="stylesheet" href="css/style-noscript.css" />
        </noscript>
        <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
        <!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->

    </head>

    <body class="no-sidebar">

        <!-- Header -->
        <header id="header" class="skel-layers-fixed">
            <h1 id="logo"><a href="indexadmin.jsp">ASSET MANAGEMENT LABORATORIUM MANAJEMEN SISTEM INFORMASI</a></h1>
            <nav id="nav">
                <ul>
                    <li class="current"><a href="indexadmin.jsp">Home</a></li>
                    <li class="submenu">
                        <a href="">Menu</a>
                        <ul>
                            <!--<li><a href="left-sidebar.html">Left Sidebar</a></li>  Left Sidebar -->
                            <!--<li><a href="right-sidebar.html">Right Sidebar</a></li> Right Sidebar -->
                            <li><a href="cekinventoryadmin.jsp">Daftar Aset</a></li>
                            <li><a href="cekstatusadmin.jsp">Cek Status Pelaporan</a></li>
                            <li><a href="record.jsp">Record Activity</a></li>

                        </ul>
                    </li>

                </ul>
            </nav>
        </header>

        <!-- Main -->

        <article id="main">
            <header class="special container">
                <span class="icon fa-mobile"></span>
                <h2><strong>Silahkan Login dengan Akun yang Telah Terdaftar</strong></h2>
            </header>

            <!-- One -->
            <section class="wrapper style4 container2">

                <!-- Content -->
                <div class="container">


                    <div class="row">
                        <div class="span6">

                            <form class="form-horizontal well" action="loginadmin/loginadmin.jsp" method="post">
                                <fieldset>
                                    <H2>Log in</H2>
                                    <div class="control-group">
                                        <label class="control-label" for="username">Username</label>

                                        <div class="controls">
                                            <input type="text" name="username" id="user" placeholder="Username">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label" for="password">Password</label>

                                        <div class="controls">
                                            <input type="password" name="password" id="Password" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="controls">
                                            <label class="checkbox">
                                                <input type="checkbox"> Remember Me
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <input type="submit" class="button special" value="Login" /> 
                                    </div>

                                </fieldset>
                            </form>
                        </div>
                    </div>





                    </body>
                    </html>