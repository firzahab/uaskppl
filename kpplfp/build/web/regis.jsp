<!DOCTYPE HTML>

<!--
        Twenty by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->





<head>
    <title>Login dan Registrasi</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.dropotron.min.js"></script>
    <script src="js/jquery.scrolly.min.js"></script>
    <script src="js/jquery.scrollgress.min.js"></script>
    <script src="js/skel.min.js"></script>
    <script src="js/skel-layers.min.js"></script>
    <script src="js/init.js"></script>
    <noscript>
    <link rel="stylesheet" href="css/skel.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/style-wide.css" />
    <link rel="stylesheet" href="css/style-noscript.css" />
    </noscript>
    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
    <!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->

</head>

<body class="no-sidebar">

    <!-- Header -->
    <header id="header" class="skel-layers-fixed">
        <h1 id="logo"><a href="index.jsp">Asset Management Laboratorium Manajemen Sistem Informasi</a></h1>
        <nav id="nav">
            <ul>
                <li class="current"><a href="index.jsp">Home</a></li>
                <li class="submenu">
                    <a href="">Menu</a>
                    <ul>
                        <!--<li><a href="left-sidebar.html">Left Sidebar</a></li>  Left Sidebar -->
                        <!--<li><a href="right-sidebar.html">Right Sidebar</a></li> Right Sidebar -->
                        <li><a href="about.jsp">Tentang JSI</a></li>
                        <li><a href="cekinventory.jsp">Daftar Aset</a></li>
                        <li><a href="indexform2.jsp">Formulir Pelaporan Aset</a></li> <!--No Sidebar -->
                        <li><a href="cekstatus.jsp">Cek Status Pelaporan</a></li>

                    </ul>
                </li>
                <%
                    if ((session.getAttribute("user") == null)) {
                %>
                <li><a href="login.jsp" class="button special">Login</a></li>
                    <%} else {
                    %>
                <li><a href="./login/logout.jsp" class="button special">Logout</a></li>
                    <%
                        }
                    %>

            </ul>
        </nav>
    </header>

    <!-- Main -->

    <article id="main">
        <header class="special container">
            <span class="icon fa-mobile"></span>
            <h2><strong>Registrasikan diri anda sekarang!</strong></h2>
        </header>

        <!-- One -->
        <section class="wrapper style4 container2">

            <!-- Content -->
            <div class="container">


                <div class="row">
                    <div class="span6">

                        <form class="form-horizontal well" action="login/registration.jsp" method="post">
                            <fieldset>
                                <H2>Registrasi</H2>
                                <div class="control-group">
                                    <label class="control-label" for="user">Username</label>

                                    <div class="controls">
                                        <input type="text" name="user" id="pass" placeholder="username">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="Password">Password</label>

                                    <div class="controls">
                                        <input type="password" name="Password" id="pass" placeholder="Password">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="nama">Nama lengkap</label>

                                    <div class="controls">
                                        <input type="text" name="nama" id="inputemail" placeholder="nama lengkap">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="nrp">NRP</label>

                                    <div class="controls">
                                        <input type="text" name="nrp" id="pass" placeholder="NRP">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="email">Email</label>

                                    <div class="controls">
                                        <input type="text" name="email" id="pass" placeholder="email">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="notelp">Nomor Telepon</label>

                                    <div class="controls">
                                        <input type="text" name="notelp" id="pass" placeholder="nomor telepon">
                                    </div>
                                </div>     
                                <div class="control-group">
                                    <div class="controls">

                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input type="submit" class="button special" value="Register" /> 
                                    <!--<button type="submit" class="btn btn-primary">Login</button>-->
                                    <!-- <button type="button" class="btn">Cancel</button>-->
                                </div>

                            </fieldset>
                        </form>
                    </div>
                </div>





                </body>
                </html>