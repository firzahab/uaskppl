<!DOCTYPE HTML>
<!--
        Twenty by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
    <head>
        <title>Tempat Jurusan Sistem Informasi</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.dropotron.min.js"></script>
        <script src="js/jquery.scrolly.min.js"></script>
        <script src="js/jquery.scrollgress.min.js"></script>
        <script src="js/skel.min.js"></script>
        <script src="js/skel-layers.min.js"></script>
        <script src="js/init.js"></script>
        <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
        <link rel="stylesheet" href="css/style-noscript.css" />
        </noscript>
    </head>
    <body class="no-sidebar">

        <!-- Header -->
        <header id="header" class="skel-layers-fixed">
            <h1 id="logo"><a href="index.jsp">ASSET MANAGEMENT LABORATORIUM MANAJEMEN SISTEM INFORMASI</a></h1>
            <nav id="nav">
                <ul>
                    <li class="current"><a href="index.jsp">Home</a></li>
                    <li class="submenu">
                        <a href="">Menu</a>
                        <ul>
                            <!--<li><a href="left-sidebar.html">Left Sidebar</a></li>  Left Sidebar -->
                            <!--<li><a href="right-sidebar.html">Right Sidebar</a></li> Right Sidebar -->
                            <li><a href="about.jsp">Tentang JSI</a></li>
                            <li><a href="cekinventory.jsp">Daftar Aset</a></li>
                            <li><a href="indexform2.jsp">Formulir Pelaporan Aset</a></li> <!--No Sidebar -->
                            <li><a href="cekstatus.jsp">Cek Status Pelaporan</a></li>


                        </ul>
                    </li>
                    <%
                        if ((session.getAttribute("user") == null)) {
                    %>
                    <li><a href="login.jsp" class="button special">Login</a></li>
                        <%} else {
                        %>
                    <li><a href="./login/logout.jsp" class="button special">Logout</a></li>
                        <%
                            }
                        %>


                </ul>
            </nav>
        </header>

        <!-- Main -->
        <article id="main">

            <header class="special container">
                <span class="icon fa-mobile"></span>
                <h2>Mari intip <strong>Dua Jenis Aset yang ada di Lab Manajemen Sistem Informasi di JSI</strong></h2>
            </header>

            <!-- One -->
            <section class="wrapper style3 container special">

                <div class="row">
                    <div class="6u 12u(narrower)">

                        <section>
                            <a href="#" class="image featured"><img src="images/kelas.jpg" alt="" /></a>
                            <header>
                                <h3>Inventaris</h3>
                            </header>
                            <p>Jenis aset <strong>inventaris </strong> merupakan aset yang memiliki jangka waktu penggunaan yang lama. Contoh jenis aset inventoris antara lain Komputer, Meja, Kursi, LCD, Layar Proyektor, Microphone, Speaker, dll.</p>
                        </section>

                    </div>
                    <div class="6u 12u(narrower)">

                        <section>
                            <a href="#" class="image featured"><img src="images/aula.jpg" alt="" /></a>
                            <header>
                                <h3>Barang Habis Pakai</h3>
                            </header>
                            <p>Jenis aset <strong>Barang habis pakai </strong>merupakan aset yang memiliki jangkau waktu penggunaan yang pendek. Contoh jenis aset habis pakai antara lain Kertas, Alat Tulis Kantor (ATK), dll.</p>
                        </section>

                    </div>
                </div>


            </section>

        </article>

        <!-- Footer -->
        <footer id="footer">


            <ul class="copyright">
                <li><center>&copy; 2015 Institut Teknologi Sepuluh Nopember, Fakultas Teknologi Informasi, Jurusan Sistem Informasi</center></li>
            </ul>

        </footer>

    </body>
</html>