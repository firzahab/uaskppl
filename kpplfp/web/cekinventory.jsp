<!DOCTYPE HTML>

<!--
        Twenty by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->

<html>
    <%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%@page import="java.sql.*"%>
    <%@page import="java.io.*"%>

    <head>
        <title>Daftar Inventory</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.dropotron.min.js"></script>
        <script src="js/jquery.scrolly.min.js"></script>
        <script src="js/jquery.scrollgress.min.js"></script>
        <script src="js/skel.min.js"></script>
        <script src="js/skel-layers.min.js"></script>
        <script src="js/init.js"></script>
        <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
        <link rel="stylesheet" href="css/style-noscript.css" />
        </noscript>
        <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
        <!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->

    </head>

    <body class="no-sidebar">

        <!-- Header -->
        <header id="header" class="skel-layers-fixed">
            <h1 id="logo"><a href="index.jsp">ASSET MANAGEMENT LABORATORIUM MANAJEMEN SISTEM INFORMASI</a></h1>
            <nav id="nav">
                <ul>
                    <li class="current"><a href="index.jsp">Home</a></li>
                    <li class="submenu">
                        <a href="">Menu</a>
                        <ul>
                            <!--<li><a href="left-sidebar.html">Left Sidebar</a></li>  Left Sidebar -->
                            <!--<li><a href="right-sidebar.html">Right Sidebar</a></li> Right Sidebar -->
                            <li><a href="about.jsp">Tentang JSI</a></li>
                            <li><a href="cekinventory.jsp">Daftar Aset</a></li>
                            <li><a href="indexform2.jsp">Formulir Pelaporan Aset</a></li> <!--No Sidebar -->
                            <li><a href="cekstatus.jsp">Cek Status Pelaporan</a></li>

                        </ul>
                    </li>
                    <%
                        if ((session.getAttribute("user") == null)) {
                    %>
                    <li><a href="login.jsp" class="button special">Login</a></li>
                        <%} else {
                        %>
                    <li><a href="./login/logout.jsp" class="button special">Logout</a></li>
                        <%
                            }
                        %>	
                </ul>
            </nav>
        </header>

        <!-- Main -->

        <article id="main">

            <header class="special container">
                <span class="icon fa-mobile"></span>
                <h2><strong>Daftar Aset</strong></h2>
            </header>

            <!-- One -->
            <section class="wrapper style4 container2">

                <!-- Content -->
                <div class="content">
                    <%
                        try {

                    //deklarasi url database
                            String url = "jdbc:mysql://localhost:3306/kppl";
                            Connection con = null;
                            Statement stat = null;
                            ResultSet rs = null;

                    //load jdbc driver
                            Class.forName("com.mysql.jdbc.Driver").newInstance();

                            con = DriverManager.getConnection(url, "root", "");

                            stat = con.createStatement();

                    //membuat query
                            String query = "select * from inventory";

                            rs = stat.executeQuery(query);

                    %>



                    <table id="tblData-table" border="3%" width="110%" class="order-table">
                        <tr>
                            <td width="10%"><b><center>Kode Barang</center></b></td>
                            <td width="10%"><b><center>Nama Barang</center></b></td>
                            <td width="20%"><b><center>Jenis Barang</center></b></td>
                            <td width="10%"><b><center>Start Date</center></b></td>
                            <td width="10%"><b><center>Exp Date</center><b></td>
                                        <td width="15%"><b><center>Status</center></b></td>

                                        </tr>
                                        <% while (rs.next()) {
                                        %>
                                        <tr>
                                            <td><%=rs.getString(2)%></td>
                                            <td><%=rs.getString(3)%></td>
                                            <td><%=rs.getString(4)%></td>
                                            <td><%=rs.getString(5)%></td>
                                            <td><%=rs.getString(6)%></td>
                                            <td><%=rs.getString(7)%></td>

                                        </tr>
                                        <%
                                            }
                                        %>
                                        <%
                                                //menutup koneksi
                                                rs.close();
                                                stat.close();
                                                con.close();
                                            } catch (Exception ex) {
                                                out.println("Unable to connect to database");
                                            }
                                        %>
                                        </table>                    
                                        </div>


                                        </body>
                                        </html>