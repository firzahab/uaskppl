<%-- 
    Document   : indexx
    Created on : Dec 24, 2015, 3:45:45 PM
    Author     : SARAHPUTRI
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Sistem Informasi</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery.dropotron.min.js"></script>
        <script src="js/jquery.scrolly.min.js"></script>
        <script src="js/jquery.scrollgress.min.js"></script>
        <script src="js/skel.min.js"></script>
        <script src="js/skel-layers.min.js"></script>
        <script src="js/init.js"></script>
        <noscript>
        <link rel="stylesheet" href="css/skel.css" />
        <link rel="stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="css/style-wide.css" />
        <link rel="stylesheet" href="css/style-noscript.css" />
        </noscript>
        
    </head>
    <body class="index">

        <!-- Header -->
        <header id="header" class="alt">
            <h1 id="logo"><a href="index.jsp"> Asset Management Laboratorium Manajemen Sistem Informasi</a></h1>
            <nav id="nav">
                <ul>
                    <li class="current"><a href="index.jsp">Home</a></li>
                    <li class="submenu">
                        <a href="">Menu</a>
                        <ul>

                            <li><a href="about.jsp">Tentang JSI</a></li>
                            <li><a href="cekinventory.jsp">Daftar Aset</a></li>
                            <li><a href="indexform2.jsp">Formulir Pelaporan Aset</a></li> <!--No Sidebar -->
                            <li><a href="cekstatus.jsp">Cek Status Pelaporan</a></li>


                        </ul>
                    </li> 

                    <%
                        if ((session.getAttribute("user") == null)) {
                    %>
                    <li><a href="login.jsp" class="button special">Login</a></li>
                        <%} else {
                        %>
                    <li><a href="./login/logout.jsp" class="button special">Logout</a></li>
                        <%
                            }
                        %>


                </ul>
            </nav>
        </header>

        <!-- Banner -->
        <section id="banner">

            <div class="inner">

                <header>
                    <h2>JURUSAN SISTEM INFORMASI</h2>
                </header>
                <p>Asset Management <strong> Laboratorium Manajemen Sistem Informasi</strong> 
                    <br/>
                    Jurusan Sistem Informasi Institut Teknologi Sepuluh Nopember
                    <br />
                    Need More Info?
                    <br />

                <footer>
                    <ul class="buttons vertical">
                        <li><a href="#main" class="button fit scrolly">Tell Me More ! :)</a></li>
                    </ul>
                </footer>

            </div>

        </section>

        <!-- Main -->
        <article id="main">

            <header class="special container">
                <span class="icon fa-bar-chart-o"></span>
                <h2>Apa itu <strong>Sistem Informasi Asset Management?</strong
                    <br />
                </h2>
                <p>Sistem Informasi ini akan memudahkan Mahasiswa Lab MSI Jurusan Sistem Informasi Institut Teknologi Sepuluh Nopember
                    <br />
                    untuk mengetahui daftar aset yang dimiliki Lab MSI 
                    <br />
                    dan mahasiswa dapat melaporkan kondisi aset secara online.
                    <br/>
                    Untuk pelaporan, mahasiswa harus
                    <a href="login.html">Login</a> terlebih dahulu. Selamat mencoba!</p>
            </header>

            <!-- Three -->
            <section class="wrapper style3 container special">

                <header class="major">
                    <h2>Mari intip <strong>Dua Jenis Aset yang ada di Lab Manajemen Sistem Informasi di JSI</strong></h2>
                </header>

                <div class="row">
                    <div class="6u 12u(narrower)">

                        <section>
                            <a href="#" class="image featured"><img src="images/inventaris.jpg" alt="" /></a>
                            <header>
                                <h3>Inventaris</h3>
                            </header>
                            <p>Jenis aset <strong>inventaris </strong> merupakan aset yang memiliki jangka waktu penggunaan yang lama. Contoh jenis aset inventoris antara lain Komputer, Meja, Kursi, LCD, Layar Proyektor, Microphone, Speaker, dll.</p>
                        </section>

                    </div>
                    <div class="6u 12u(narrower)">

                        <section>
                            <a href="#" class="image featured"><img src="images/ATK.jpg" alt="" /></a>
                            <header>
                                <h3>Barang Habis Pakai</h3>
                            </header>
                            <p>Jenis aset <strong>Barang habis pakai </strong>merupakan aset yang memiliki jangkau waktu penggunaan yang pendek. Contoh jenis aset habis pakai antara lain Kertas, Alat Tulis Kantor (ATK), dll.</p>
                        </section>

                    </div>
                </div>


                <footer class="major">
                    <ul class="buttons">
                        <li><a href="tempat.jsp" class="button">See More</a></li>
                    </ul>
                </footer>

            </section>

        </article>

        <!-- Footer -->
        <footer id="footer">


            <ul class="copyright">
                <li><center>&copy; 2015 Institut Teknologi Sepuluh Nopember, Fakultas Teknologi Informasi, Jurusan Sistem Informasi</center></li>
            </ul>

        </footer>

    </body>
</html>
